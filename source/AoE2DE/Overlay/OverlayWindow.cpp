///////////////////////////////////////////////////////////////////////////////////////////////////
// Overlay Window Implementation                                                                 //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <AoE2DE/Overlay/OverlayWindow.hpp>

// -------------------------------------------------------------------------------- Library Headers
#include <Network/Twitch/EventSub/Events/ChannelAdBreakBeginEvent.hpp>
#include <Network/Twitch/EventSub/Events/ChannelPointsCustomRewardRedemptionAddEvent.hpp>
#include <Network/Twitch/EventSub/Events/ChannelRaidEvent.hpp>
#include <Network/Twitch/Extensions/Heat/Events/UserClickEvent.hpp>
#include <Network/Twitch/IRC/Events/UserMessageEvent.hpp>

#include <glad/gl.h>

#include <glm/gtc/matrix_transform.hpp>

#include <imgui.h>

// --------------------------------------------------------------------------------- System Headers
#include <algorithm>
#include <cmath>
#include <functional>

namespace AoE2DE::Overlay
{

OverlayWindow::OverlayWindow() :
    OpenGLWindow(4, 6, 1, "AoE2DE Overlay"),
    mTwitchAPISubscription(Network::Core::Dispatcher::Instance().Subscribe<Network::Twitch::API::Event>(this)),
    mTwitchEventSubSubscription(Network::Core::Dispatcher::Instance().Subscribe<Network::Twitch::EventSub::Event>(this)),
    mTwitchHeatSubscription(Network::Core::Dispatcher::Instance().Subscribe<Network::Twitch::Extensions::Heat::Event>(this)),
    mTwitchIRCSubscription(Network::Core::Dispatcher::Instance().Subscribe<Network::Twitch::IRC::Event>(this)),
    mRandom(std::random_device{}())
{}

OverlayWindow::~OverlayWindow()
{
    mTwitchAPISubscription.Revoke();
    mTwitchEventSubSubscription.Revoke();
    mTwitchHeatSubscription.Revoke();
    mTwitchIRCSubscription.Revoke();
}

bool OverlayWindow::OnCreate()
{
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Load assets                                                                               //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    mUnitShader = std::make_unique<AoE2DE::Overlay::Graphics::OpenGLUnitShader>();
    if (mUnitShader->IsNull())
    {
        return false;
    }

    const AoE2DE::Core::Media::SLDTexture::LoadOptions lUnitLoadOptions{
        .Directions{ 16ULL },
        .CopyMode{ AoE2DE::Core::Media::SLDTexture::LoadOptions::CopyModeOptions::PreviousFrameInAnimation }
    };
    AoE2DE::Core::Media::SLDTexture lUnitTexture;

    // TODO: figure out why the decoding of the teutonic knight is bugged.
    for (auto&& lUnit : {
        std::make_tuple(UnitClass::WarElephant, UnitAnimation::Idle, std::string("assets/sld/u_ele_war_elephant_idleA_x1.sld")),
        std::make_tuple(UnitClass::WarElephant, UnitAnimation::Walk, std::string("assets/sld/u_ele_war_elephant_walkA_x1.sld")),
        std::make_tuple(UnitClass::ThrowingAxeman, UnitAnimation::Idle, std::string("assets/sld/u_inf_throwingaxeman_idleA_x1.sld")),
        std::make_tuple(UnitClass::ThrowingAxeman, UnitAnimation::Walk, std::string("assets/sld/u_inf_throwingaxeman_walkA_x1.sld")),
        std::make_tuple(UnitClass::ThrowingAxeman, UnitAnimation::Death, std::string("assets/sld/u_inf_throwingaxeman_deathA_x1.sld")),
        std::make_tuple(UnitClass::ThrowingAxeman, UnitAnimation::Decay, std::string("assets/sld/u_inf_throwingaxeman_decayA_x1.sld")),
        std::make_tuple(UnitClass::EliteEagleWarrior, UnitAnimation::Walk, std::string("assets/sld/u_inf_eliteeaglewarrior_walkA_x1.sld")),
        std::make_tuple(UnitClass::TradeCart, UnitAnimation::Walk, std::string("assets/sld/u_trade_cart_west_walkA_x1.sld")) })
    {
        if (!lUnitTexture.LoadFromFile(std::get<std::string>(lUnit), lUnitLoadOptions))
        {
            return false;
        }
        if (auto lTexture{ std::make_unique<AoE2DE::Overlay::Graphics::OpenGLUnitTexture>(lUnitTexture) }; !lTexture->IsNull())
        {
            mUnitBatchInfo[GetUnitBatchIndex(std::get<UnitClass>(lUnit), std::get<UnitAnimation>(lUnit))].Texture = std::move(lTexture);
        }

        // TODO: remove this wasteful switch. It's bad.
        // TODO: same speed for every unit: it's bad.
        switch (std::get<UnitAnimation>(lUnit))
        {
        case UnitAnimation::Idle:
            mUnitClassInfo[std::get<UnitClass>(lUnit)].IdleAnimationInfo.Duration = 2.0f;
            mUnitClassInfo[std::get<UnitClass>(lUnit)].IdleAnimationInfo.NumberOfOrientations = 16ULL;
            mUnitClassInfo[std::get<UnitClass>(lUnit)].IdleAnimationInfo.NumberOfFrames = lUnitTexture.GetFrameCount();
            break;
        case UnitAnimation::Walk:
            mUnitClassInfo[std::get<UnitClass>(lUnit)].WalkSpeed = 80.0f;
            mUnitClassInfo[std::get<UnitClass>(lUnit)].WalkAnimationInfo.Duration = 0.7f;
            mUnitClassInfo[std::get<UnitClass>(lUnit)].WalkAnimationInfo.NumberOfOrientations = 16ULL;
            mUnitClassInfo[std::get<UnitClass>(lUnit)].WalkAnimationInfo.NumberOfFrames = lUnitTexture.GetFrameCount();
            break;
        case UnitAnimation::Death:
            mUnitClassInfo[std::get<UnitClass>(lUnit)].DeathAnimationInfo.Duration = 0.7f;
            mUnitClassInfo[std::get<UnitClass>(lUnit)].DeathAnimationInfo.NumberOfOrientations = 16ULL;
            mUnitClassInfo[std::get<UnitClass>(lUnit)].DeathAnimationInfo.NumberOfFrames = lUnitTexture.GetFrameCount();
            break;
        case UnitAnimation::Decay:
            mUnitClassInfo[std::get<UnitClass>(lUnit)].DecayAnimationInfo.Duration = 10.0f;
            mUnitClassInfo[std::get<UnitClass>(lUnit)].DecayAnimationInfo.NumberOfOrientations = 16ULL;
            mUnitClassInfo[std::get<UnitClass>(lUnit)].DecayAnimationInfo.NumberOfFrames = lUnitTexture.GetFrameCount();
            break;
        }
    }

    mUnitClassInfo[UnitClass::EliteEagleWarrior].WalkAnimationInfo.Duration = 2.0f;
    mUnitClassInfo[UnitClass::EliteEagleWarrior].WalkSpeed = 40.0f;

    const float lFramebufferWidth{ static_cast<float>(GetFramebufferWidth()) };
    const float lFramebufferHeight{ static_cast<float>(GetFramebufferHeight()) };

    mStreamAvatar.Instance.Class = UnitClass::WarElephant;
    mStreamAvatar.Instance.Animation = UnitAnimation::Idle;
    mStreamAvatar.Instance.Color = glm::vec3(1.0f, 0.0f, 0.0f);
    mStreamAvatar.Instance.Scale = 1.0f;
    mStreamAvatar.Instance.Sampler.Reset(&mUnitClassInfo[mStreamAvatar.Instance.Class].IdleAnimationInfo);
    mStreamAvatar.TargetPosition = glm::vec2(lFramebufferWidth / 2.0f, lFramebufferHeight / 2.0f);
    mStreamAvatar.Instance.Position = glm::vec2(lFramebufferWidth / 2.0f, lFramebufferHeight / 2.0f);

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Setup unit attribute buffers                                                              //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    const GLfloat lQuadVertexBufferData[] =
    {
        0.0f, 0.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 1.0f, 1.0f,
        0.0f, 1.0f, 0.0f, 0.0f,
        1.0f, 1.0f, 1.0f, 0.0f
    };

    const GLuint lQuadElementBufferData[] =
    {
        0, 1, 3, 0, 3, 2
    };

    glCreateVertexArrays(1, &mUnitVertexArrayHandle);

    glCreateBuffers(1, &mUnitVertexBufferHandle);
    glNamedBufferStorage(mUnitVertexBufferHandle, sizeof(lQuadVertexBufferData), lQuadVertexBufferData, 0);
    glVertexArrayVertexBuffer(mUnitVertexArrayHandle, 0, mUnitVertexBufferHandle, 0, 4 * sizeof(GLfloat));
    glVertexArrayBindingDivisor(mUnitVertexArrayHandle, 0, 0);

    glEnableVertexArrayAttrib(mUnitVertexArrayHandle, 0);
    glVertexArrayAttribFormat(mUnitVertexArrayHandle, 0, 2, GL_FLOAT, GL_FALSE, 0);
    glVertexArrayAttribBinding(mUnitVertexArrayHandle, 0, 0);

    glEnableVertexArrayAttrib(mUnitVertexArrayHandle, 1);
    glVertexArrayAttribFormat(mUnitVertexArrayHandle, 1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat));
    glVertexArrayAttribBinding(mUnitVertexArrayHandle, 1, 0);

    glCreateBuffers(1, &mUnitElementBufferHandle);
    glNamedBufferStorage(mUnitElementBufferHandle, sizeof(lQuadElementBufferData), lQuadElementBufferData, 0);
    glVertexArrayElementBuffer(mUnitVertexArrayHandle, mUnitElementBufferHandle);

    glCreateBuffers(sInstanceBufferCount, mInstanceBufferHandle);
    for (std::size_t lBufferIndex{}; lBufferIndex < sInstanceBufferCount; lBufferIndex++)
    {
        glNamedBufferData(mInstanceBufferHandle[lBufferIndex], 0, 0, GL_DYNAMIC_DRAW);
    }
    glVertexArrayBindingDivisor(mUnitVertexArrayHandle, 1, 1);

    // position (x, y) + z = frame index + s = scaling factor
    glEnableVertexArrayAttrib(mUnitVertexArrayHandle, 2);
    glVertexArrayAttribFormat(mUnitVertexArrayHandle, 2, 4, GL_FLOAT, GL_FALSE, 0);
    glVertexArrayAttribBinding(mUnitVertexArrayHandle, 2, 1);

    // color (r, g, b)
    glEnableVertexArrayAttrib(mUnitVertexArrayHandle, 3);
    glVertexArrayAttribFormat(mUnitVertexArrayHandle, 3, 3, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat));
    glVertexArrayAttribBinding(mUnitVertexArrayHandle, 3, 1);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Initialize timers                                                                         //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    const auto lNow{ std::chrono::steady_clock::now() };
    mAdBreakEndTime = lNow;
    mAdBreakNextUnitSpawnTime = lNow;
    return true;
}

bool OverlayWindow::OnUpdate(
    double pCurrentTime,
    double pElapsedTime)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // TODO: add reward to disable clearing the color buffer for a fixed duration

    const auto lCurrentTime{ std::chrono::steady_clock::now() };
    const float lFramebufferWidth{ static_cast<float>(GetFramebufferWidth()) };
    const float lFramebufferHeight{ static_cast<float>(GetFramebufferHeight()) };

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // GUI settings                                                                              //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    ImGuiShowWindowSettings();

    ImGui::DragFloat("Simulation Speed", &mSimulationSpeed, 0.1f, 0.1f, 10.0f, "%f");
    ImGui::DragFloat("Stream Avatar Speed", &mStreamAvatar.Instance.Speed, 0.1f, 0.1f, 10.0f, "%f");

    if (ImGui::Button("Test Ad Break animation"))
    {
        mAdBreakEndTime = std::chrono::steady_clock::now() + std::chrono::seconds(30);
    }
    if (ImGui::Button("Test Adge of Empires animation"))
    {
        mAdgeOfEmpiresEndTime = std::chrono::steady_clock::now() + std::chrono::seconds(30);
    }
    if (ImGui::Button("Test Raid animation"))
    {
        mRaidEndTime = std::chrono::steady_clock::now() + std::chrono::seconds(30);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Update the unit data                                                                      //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    auto UpdateUnitAvatar{
        [this](Avatar& pAvatar, const glm::vec2& pTargetPath)
        {
            if (glm::length(pTargetPath))
            {
                if (std::exchange(pAvatar.Instance.Animation, UnitAnimation::Walk) != UnitAnimation::Walk)
                {
                    pAvatar.Instance.Sampler.Animation = &mUnitClassInfo[pAvatar.Instance.Class].WalkAnimationInfo;
                    pAvatar.Instance.Sampler.Offset = 0.0f;
                }
                pAvatar.Instance.Sampler.SetOrientation(pTargetPath.x, pTargetPath.y);
            }
            else
            {
                if (std::exchange(pAvatar.Instance.Animation, UnitAnimation::Idle) != UnitAnimation::Idle)
                {
                    pAvatar.Instance.Sampler.Animation = &mUnitClassInfo[pAvatar.Instance.Class].IdleAnimationInfo;
                    pAvatar.Instance.Sampler.Offset = 0.0f;
                }
            }
        }
    };

    // -------------------------------------------------------------------------------------------- Update Ad Break Animation
    {
        std::unique_lock<std::mutex> lGuard{ mMutex }; // lock timers

        constexpr int lTradeCartUnitScreenPadding{ 100 };
        if (lCurrentTime < mAdBreakEndTime && lCurrentTime > mAdBreakNextUnitSpawnTime)
        {
            mAdBreakNextUnitSpawnTime = lCurrentTime + std::chrono::seconds(1);

            std::uniform_int_distribution lHeightDistribution{ 1, 9 };

            UnitInstanceInfo lTradeCartUnit{};
            lTradeCartUnit.Class = UnitClass::TradeCart;
            lTradeCartUnit.Animation = UnitAnimation::Walk;
            lTradeCartUnit.Color = glm::vec3(0.0f, 1.0f, 0.0f);
            lTradeCartUnit.Scale = 1.0f;
            lTradeCartUnit.Sampler.Reset(&mUnitClassInfo[lTradeCartUnit.Class].WalkAnimationInfo);
            lTradeCartUnit.Position = glm::vec2(static_cast<float>(-lTradeCartUnitScreenPadding), lFramebufferHeight * 0.1 * static_cast<float>(lHeightDistribution(mRandom)));
            lTradeCartUnit.Speed = 2.0f;
            mTradeUnits.push_back(lTradeCartUnit);
        }

        if (lCurrentTime < mAdgeOfEmpiresEndTime && lCurrentTime > mAdgeOfEmpiresNextUnitSpawnTime)
        {
            mAdgeOfEmpiresNextUnitSpawnTime = lCurrentTime + std::chrono::milliseconds(100);

            std::uniform_int_distribution lHeightDistribution{ 1, 19 };

            UnitInstanceInfo lTradeCartUnit{};
            lTradeCartUnit.Class = UnitClass::TradeCart;
            lTradeCartUnit.Animation = UnitAnimation::Walk;
            lTradeCartUnit.Color = mAdgeOfEmpiresColor;
            lTradeCartUnit.Scale = 1.0f;
            lTradeCartUnit.Sampler.Reset(&mUnitClassInfo[lTradeCartUnit.Class].WalkAnimationInfo);
            lTradeCartUnit.Position = glm::vec2(static_cast<float>(-lTradeCartUnitScreenPadding), lFramebufferHeight * 0.05 * static_cast<float>(lHeightDistribution(mRandom)));
            lTradeCartUnit.Speed = 3.0f;
            mTradeUnits.push_back(lTradeCartUnit);
        }

        for (auto& lTradeCartUnit : mTradeUnits)
        {
            // Update the animation.

            lTradeCartUnit.Sampler.Speed = mSimulationSpeed * lTradeCartUnit.Speed;
            lTradeCartUnit.Sampler.Update(pElapsedTime);

            // Update the position.

            const float lDistance{ static_cast<float>(pElapsedTime * mSimulationSpeed * lTradeCartUnit.Speed * mUnitClassInfo[lTradeCartUnit.Class].WalkSpeed) };
            lTradeCartUnit.Position.x += lDistance;
        }

        mTradeUnits.erase(std::remove_if(mTradeUnits.begin(), mTradeUnits.end(),
            [lFramebufferWidth, lTradeCartUnitScreenPadding](const UnitInstanceInfo& lTradeUnit)
            {
                return lTradeUnit.Position.x > lFramebufferWidth + lTradeCartUnitScreenPadding;
            }), mTradeUnits.end());
    }

    // -------------------------------------------------------------------------------------------- Update Raid Animation
    {
        std::unique_lock<std::mutex> lGuard{ mMutex }; // lock timers

        constexpr int lRaidUnitScreenPadding{ 40 };
        if (lCurrentTime < mRaidEndTime && lCurrentTime > mRaidNextUnitSpawnTime)
        {
            mRaidNextUnitSpawnTime = lCurrentTime + std::chrono::milliseconds(100);

            std::uniform_int_distribution lWidthDistribution{ lRaidUnitScreenPadding, static_cast<int>(lFramebufferWidth) - lRaidUnitScreenPadding };

            for (int i = 0; i < 8; i++)
            {
                UnitInstanceInfo lRaidingUnit{};
                lRaidingUnit.Class = UnitClass::EliteEagleWarrior;
                lRaidingUnit.Animation = UnitAnimation::Walk;
                lRaidingUnit.Color = glm::vec3(1.0f, 0.0f, 1.0f);
                lRaidingUnit.Scale = 1.0f;
                lRaidingUnit.Sampler.Reset(&mUnitClassInfo[lRaidingUnit.Class].WalkAnimationInfo);
                lRaidingUnit.Sampler.Orientation = 4;
                lRaidingUnit.Position = glm::vec2(static_cast<float>(lWidthDistribution(mRandom)), lFramebufferHeight);
                lRaidingUnit.Speed = 3.0f;
                mRaidUnits.push_back(lRaidingUnit);
            }
        }

        for (auto& lRaidingUnit : mRaidUnits)
        {
            // Update the animation.

            lRaidingUnit.Sampler.Speed = mSimulationSpeed * lRaidingUnit.Speed;
            lRaidingUnit.Sampler.Update(pElapsedTime);

            // Update the position.

            const float lDistance{ static_cast<float>(pElapsedTime * mSimulationSpeed * lRaidingUnit.Speed * mUnitClassInfo[lRaidingUnit.Class].WalkSpeed) };
            lRaidingUnit.Position.y -= lDistance;
        }

        mRaidUnits.erase(std::remove_if(mRaidUnits.begin(), mRaidUnits.end(),
            [lFramebufferWidth](const UnitInstanceInfo& lTradeUnit)
            {
                return lTradeUnit.Position.y < -200.0f;
            }), mRaidUnits.end());
    }

    // -------------------------------------------------------------------------------------------- Update Stream Avatar
    {
        std::unique_lock<std::mutex> lGuard{ mMutex }; // lock target position

        const glm::vec2 lPathToTravel{ mStreamAvatar.TargetPosition - mStreamAvatar.Instance.Position };

        // Update the animation.

        UpdateUnitAvatar(mStreamAvatar, lPathToTravel);
        mStreamAvatar.Instance.Sampler.Speed = mSimulationSpeed * mStreamAvatar.Instance.Speed;
        mStreamAvatar.Instance.Sampler.Update(pElapsedTime);

        // Update the position.

        const float lDistance{ static_cast<float>(pElapsedTime * mSimulationSpeed * mStreamAvatar.Instance.Speed * mUnitClassInfo[mStreamAvatar.Instance.Class].WalkSpeed) };
        if (lDistance >= glm::length(lPathToTravel))
        {
            mStreamAvatar.Instance.Position = mStreamAvatar.TargetPosition;
        }
        else if (glm::length(lPathToTravel) > 0.0f)
        {
            mStreamAvatar.Instance.Position += glm::normalize(lPathToTravel) * lDistance;
        }
    }

    // -------------------------------------------------------------------------------------------- Update Personal Avatars
    {
        std::unique_lock<std::mutex> lGuard{ mMutex }; // lock target position

        for (auto& [lUsername, lAvatar] : mPersonalAvatars)
        {
            const glm::vec2 lPathToTravel{ lAvatar.TargetPosition - lAvatar.Instance.Position };

            // Update the animation.

            const float lDeathAnimationDuration{ mUnitClassInfo[lAvatar.Instance.Class].DeathAnimationInfo.Duration };
            const float lDeathAndDecayAnimationDuration{ lDeathAnimationDuration + mUnitClassInfo[lAvatar.Instance.Class].DecayAnimationInfo.Duration };
            if (lCurrentTime < lAvatar.AvatarEndTime) // alive
            {
                UpdateUnitAvatar(lAvatar, lPathToTravel);
            }
            else if (lCurrentTime < lAvatar.AvatarEndTime + std::chrono::milliseconds(static_cast<int>(1000.0 * lDeathAnimationDuration))) // dying
            {
                if (std::exchange(lAvatar.Instance.Animation, UnitAnimation::Death) != UnitAnimation::Death)
                {
                    lAvatar.Instance.Sampler.Animation = &mUnitClassInfo[lAvatar.Instance.Class].DeathAnimationInfo;
                    lAvatar.Instance.Sampler.Offset = 0.0f;
                    lAvatar.Instance.Sampler.Loop = false;
                }
            }
            else if(std::exchange(lAvatar.Instance.Animation, UnitAnimation::Decay) != UnitAnimation::Decay) // decaying
            {
                lAvatar.Instance.Sampler.Animation = &mUnitClassInfo[lAvatar.Instance.Class].DecayAnimationInfo;
                lAvatar.Instance.Sampler.Offset = 0.0f;
                lAvatar.Instance.Sampler.Loop = false;
            }
            lAvatar.Instance.Sampler.Speed = mSimulationSpeed * lAvatar.Instance.Speed;
            lAvatar.Instance.Sampler.Update(pElapsedTime);

            // Update the position.

            if (lCurrentTime < lAvatar.AvatarEndTime)
            {
                const float lDistance{ static_cast<float>(pElapsedTime * mSimulationSpeed * lAvatar.Instance.Speed * mUnitClassInfo[lAvatar.Instance.Class].WalkSpeed) };
                if (lDistance >= glm::length(lPathToTravel))
                {
                    lAvatar.Instance.Position = lAvatar.TargetPosition;
                }
                else if (glm::length(lPathToTravel) > 0.0f)
                {
                    lAvatar.Instance.Position += glm::normalize(lPathToTravel) * lDistance;
                }
            }
        }

        for (auto lIt{ mPersonalAvatars.begin() }; lIt != mPersonalAvatars.end();)
        {
            const float lDeathAnimationDuration{ mUnitClassInfo[lIt->second.Instance.Class].DeathAnimationInfo.Duration };
            const float lDeathAndDecayAnimationDuration{ lDeathAnimationDuration + mUnitClassInfo[lIt->second.Instance.Class].DecayAnimationInfo.Duration };
            if (lCurrentTime >= lIt->second.AvatarEndTime + std::chrono::milliseconds(static_cast<int>(1000.0 * lDeathAndDecayAnimationDuration)))
            {
                lIt = mPersonalAvatars.erase(lIt);
            }
            else
            {
                lIt++;
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Update the batch info                                                                     //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    // TODO: this whole step can be merged with the previous step.

    for (auto& lBatchInfo : mUnitBatchInfo)
    {
        lBatchInfo.Count = 0;
    }

    // -------------------------------------------------------------------------------------------- Update Trade Units

    for (const auto& lUnitInstance : mTradeUnits)
    {
        mUnitBatchInfo[GetUnitBatchIndex(lUnitInstance.Class, lUnitInstance.Animation)].Count++;
    }

    // -------------------------------------------------------------------------------------------- Update Raid Units

    for (const auto& lUnitInstance : mRaidUnits)
    {
        mUnitBatchInfo[GetUnitBatchIndex(lUnitInstance.Class, lUnitInstance.Animation)].Count++;
    }

    // -------------------------------------------------------------------------------------------- Update Stream Avatar

    mUnitBatchInfo[GetUnitBatchIndex(mStreamAvatar.Instance.Class, mStreamAvatar.Instance.Animation)].Count++;

    // -------------------------------------------------------------------------------------------- Update Personal Avatars

    for (const auto& [lUsername, lAvatar] : mPersonalAvatars)
    {
        mUnitBatchInfo[GetUnitBatchIndex(lAvatar.Instance.Class, lAvatar.Instance.Animation)].Count++;
    }

    // -------------------------------------------------------------------------------------------- Compute the start offset for each batch

    std::size_t lOffset{};
    for (auto& lBatchInfo : mUnitBatchInfo)
    {
        lBatchInfo.Offset = lOffset;
        lOffset += lBatchInfo.Count;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Update the attribute buffer                                                               //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    const std::size_t lInstanceCount{ 1ULL + mTradeUnits.size() + mRaidUnits.size() + mPersonalAvatars.size() }; // +1 for the stream avatar
    const std::size_t lInstanceStride{ 7 };
    const std::size_t lInstanceStrideInBytes{ lInstanceStride * sizeof(GLfloat) };
    const std::size_t lInstanceBufferSize{ lInstanceCount * lInstanceStride };
    const std::size_t lInstanceBufferSizeInBytes{ lInstanceCount * lInstanceStrideInBytes };

    if (mInstanceAttributeBuffer.size() < lInstanceBufferSize)
    {
        mInstanceAttributeBuffer.resize(lInstanceBufferSize);
    }

    // -------------------------------------------------------------------------------------------- Update Trade Units

    for (const auto& lUnitInstance : mTradeUnits)
    {
        const std::size_t lOffset{ lInstanceStride * mUnitBatchInfo[GetUnitBatchIndex(lUnitInstance.Class, lUnitInstance.Animation)].Offset++ };
        mInstanceAttributeBuffer[lOffset] = lUnitInstance.Position.x;
        mInstanceAttributeBuffer[lOffset + 1] = lUnitInstance.Position.y;
        mInstanceAttributeBuffer[lOffset + 2] = lUnitInstance.Sampler.Sample();
        mInstanceAttributeBuffer[lOffset + 3] = lUnitInstance.Scale;
        mInstanceAttributeBuffer[lOffset + 4] = lUnitInstance.Color.r;
        mInstanceAttributeBuffer[lOffset + 5] = lUnitInstance.Color.g;
        mInstanceAttributeBuffer[lOffset + 6] = lUnitInstance.Color.b;
    }

    // -------------------------------------------------------------------------------------------- Update Raid Units

    for (const auto& lUnitInstance : mRaidUnits)
    {
        const std::size_t lOffset{ lInstanceStride * mUnitBatchInfo[GetUnitBatchIndex(lUnitInstance.Class, lUnitInstance.Animation)].Offset++ };
        mInstanceAttributeBuffer[lOffset] = lUnitInstance.Position.x;
        mInstanceAttributeBuffer[lOffset + 1] = lUnitInstance.Position.y;
        mInstanceAttributeBuffer[lOffset + 2] = lUnitInstance.Sampler.Sample();
        mInstanceAttributeBuffer[lOffset + 3] = lUnitInstance.Scale;
        mInstanceAttributeBuffer[lOffset + 4] = lUnitInstance.Color.r;
        mInstanceAttributeBuffer[lOffset + 5] = lUnitInstance.Color.g;
        mInstanceAttributeBuffer[lOffset + 6] = lUnitInstance.Color.b;
    }

    // -------------------------------------------------------------------------------------------- Update Stream Avatar

    {
        const std::size_t lOffset{ lInstanceStride * mUnitBatchInfo[GetUnitBatchIndex(mStreamAvatar.Instance.Class, mStreamAvatar.Instance.Animation)].Offset++ };
        mInstanceAttributeBuffer[lOffset] = mStreamAvatar.Instance.Position.x;
        mInstanceAttributeBuffer[lOffset + 1] = mStreamAvatar.Instance.Position.y;
        mInstanceAttributeBuffer[lOffset + 2] = mStreamAvatar.Instance.Sampler.Sample();
        mInstanceAttributeBuffer[lOffset + 3] = mStreamAvatar.Instance.Scale;
        mInstanceAttributeBuffer[lOffset + 4] = mStreamAvatar.Instance.Color.r;
        mInstanceAttributeBuffer[lOffset + 5] = mStreamAvatar.Instance.Color.g;
        mInstanceAttributeBuffer[lOffset + 6] = mStreamAvatar.Instance.Color.b;
    }

    // -------------------------------------------------------------------------------------------- Update Personal Avatars

    for (const auto& [lUsername, lAvatar] : mPersonalAvatars)
    {
        const std::size_t lOffset{ lInstanceStride * mUnitBatchInfo[GetUnitBatchIndex(lAvatar.Instance.Class, lAvatar.Instance.Animation)].Offset++ };
        mInstanceAttributeBuffer[lOffset] = lAvatar.Instance.Position.x;
        mInstanceAttributeBuffer[lOffset + 1] = lAvatar.Instance.Position.y;
        mInstanceAttributeBuffer[lOffset + 2] = lAvatar.Instance.Sampler.Sample();
        mInstanceAttributeBuffer[lOffset + 3] = lAvatar.Instance.Scale;
        mInstanceAttributeBuffer[lOffset + 4] = lAvatar.Instance.Color.r;
        mInstanceAttributeBuffer[lOffset + 5] = lAvatar.Instance.Color.g;
        mInstanceAttributeBuffer[lOffset + 6] = lAvatar.Instance.Color.b;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Upload the attribute buffer                                                               //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    // Select, resize and update the frame's attribute buffer.

    mCurrentInstanceBuffer = (mCurrentInstanceBuffer + 1) % sInstanceBufferCount;
    glVertexArrayVertexBuffer(mUnitVertexArrayHandle, 1, mInstanceBufferHandle[mCurrentInstanceBuffer], 0, lInstanceStrideInBytes);
    if (mInstanceBufferSize[mCurrentInstanceBuffer] < lInstanceBufferSizeInBytes)
    {
        glNamedBufferData(mInstanceBufferHandle[mCurrentInstanceBuffer], lInstanceBufferSizeInBytes, 0, GL_DYNAMIC_DRAW);
    }
    glNamedBufferSubData(mInstanceBufferHandle[mCurrentInstanceBuffer], 0, lInstanceBufferSizeInBytes, mInstanceAttributeBuffer.data());

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Drawing                                                                                   //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    mUnitShader->Use();

    std::size_t lBaseInstance{};
    for (auto& lUnitBatch : mUnitBatchInfo)
    {
        if (lUnitBatch.Count && lUnitBatch.Texture)
        {
            lUnitBatch.Texture->Bind();

            mUnitShader->SetTextureSize(glm::vec2(lUnitBatch.Texture->GetWidth(), lUnitBatch.Texture->GetHeight()));
            mUnitShader->SetTextureHotspot(glm::vec2(lUnitBatch.Texture->GetHotspotX(), lUnitBatch.Texture->GetHeight() - lUnitBatch.Texture->GetHotspotY()));
            mUnitShader->SetProjection(glm::ortho(0.0f, lFramebufferWidth, 0.0f, lFramebufferHeight, 200.0f, -lFramebufferHeight));

            glBindVertexArray(mUnitVertexArrayHandle);
            glDrawElementsInstancedBaseInstance(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0, lUnitBatch.Count, lBaseInstance);
            glBindVertexArray(0);

            lBaseInstance += lUnitBatch.Count;
        }
    }
    return true;
}

void OverlayWindow::OnDestroy()
{
    glDeleteBuffers(1, &mUnitVertexBufferHandle);
    glDeleteBuffers(1, &mUnitElementBufferHandle);
    glDeleteBuffers(sInstanceBufferCount, mInstanceBufferHandle);
    glDeleteVertexArrays(1, &mUnitVertexArrayHandle);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Window Event Handlers                                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////

void OverlayWindow::OnMouseButtonPressed(
    OpenGLWindow::MouseButton pButton,
    OpenGLWindow::KeyboardMods pMods)
{
    const float lFramebufferHeight{ static_cast<float>(GetFramebufferHeight()) };
    const float lFramebufferWidth{ static_cast<float>(GetFramebufferWidth()) };
    mStreamAvatar.TargetPosition.x = static_cast<float>(GetCursorX());
    mStreamAvatar.TargetPosition.y = lFramebufferHeight - static_cast<float>(GetCursorY());
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch Event Handlers                                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////

void OverlayWindow::OnEvent(
    const Network::Twitch::EventSub::Events::ChannelAdBreakBeginEvent& pEvent)
{
    std::unique_lock<std::mutex> lGuard{ mMutex };
    mAdBreakEndTime = std::chrono::steady_clock::now() + std::chrono::seconds(pEvent.Duration);
    mAdBreakNextUnitSpawnTime = std::chrono::steady_clock::now();
}

void OverlayWindow::OnEvent(
    const Network::Twitch::EventSub::Events::ChannelPointsCustomRewardRedemptionAddEvent& pEvent)
{
    std::unique_lock<std::mutex> lGuard{ mMutex };

    const float lFramebufferWidth{ static_cast<float>(GetFramebufferWidth()) };
    const float lFramebufferHeight{ static_cast<float>(GetFramebufferHeight()) };

    if (pEvent.RewardTitle == "Adge of Empires")
    {
        mAdgeOfEmpiresEndTime = std::chrono::steady_clock::now() + std::chrono::seconds(60);
        mAdgeOfEmpiresNextUnitSpawnTime = std::chrono::steady_clock::now();
        mAdgeOfEmpiresColor = glm::vec3(0.0f, 0.0f, 1.0f);
    }
    if (pEvent.RewardTitle == "Show Personal Avatar")
    {
        std::uniform_int_distribution lWidthDistribution{ 0, static_cast<int>(lFramebufferWidth) };
        std::uniform_int_distribution lHeightDistribution{ 0, static_cast<int>(lFramebufferHeight) };
        mPersonalAvatars[pEvent.UserId].AvatarEndTime = std::chrono::steady_clock::now() + std::chrono::seconds(30);
        mPersonalAvatars[pEvent.UserId].Instance.Position.x = lWidthDistribution(mRandom);
        mPersonalAvatars[pEvent.UserId].Instance.Position.y = lHeightDistribution(mRandom);
        mPersonalAvatars[pEvent.UserId].Instance.Class = UnitClass::ThrowingAxeman;
        mPersonalAvatars[pEvent.UserId].Instance.Animation = UnitAnimation::Idle;
        mPersonalAvatars[pEvent.UserId].Instance.Color = glm::vec3(0.0f, 1.0f, 1.0f);
        mPersonalAvatars[pEvent.UserId].Instance.Scale = 1.0f;
        mPersonalAvatars[pEvent.UserId].Instance.Sampler.Reset(&mUnitClassInfo[mPersonalAvatars[pEvent.UserId].Instance.Class].IdleAnimationInfo);
        mPersonalAvatars[pEvent.UserId].TargetPosition = mPersonalAvatars[pEvent.UserId].Instance.Position;
    }
}

void OverlayWindow::OnEvent(
    const Network::Twitch::EventSub::Events::ChannelRaidEvent& pEvent)
{
    std::unique_lock<std::mutex> lGuard{ mMutex };
    mRaidEndTime = std::chrono::steady_clock::now() + std::chrono::seconds(60);
    mRaidNextUnitSpawnTime = std::chrono::steady_clock::now();
}

void OverlayWindow::OnEvent(
    const Network::Twitch::Extensions::Heat::Events::UserClickEvent& pEvent)
{
    std::unique_lock<std::mutex> lGuard{ mMutex };
    const float lFramebufferHeight{ static_cast<float>(GetFramebufferHeight()) };
    const float lFramebufferWidth{ static_cast<float>(GetFramebufferWidth()) };

    if (auto lAvatarIt{ mPersonalAvatars.find(pEvent.UserId) }; lAvatarIt != mPersonalAvatars.cend())
    {
        lAvatarIt->second.TargetPosition.x = pEvent.CursorX * lFramebufferWidth;
        lAvatarIt->second.TargetPosition.y = lFramebufferHeight - pEvent.CursorY * lFramebufferHeight;
        lAvatarIt->second.AvatarEndTime = std::chrono::steady_clock::now() + std::chrono::seconds(180);
    }
    else
    {
        mStreamAvatar.TargetPosition.x = pEvent.CursorX * lFramebufferWidth;
        mStreamAvatar.TargetPosition.y = lFramebufferHeight - pEvent.CursorY * lFramebufferHeight;
    }
}

void OverlayWindow::OnEvent(
    const Network::Twitch::IRC::Events::UserMessageEvent& pEvent)
{
    std::unique_lock<std::mutex> lGuard{ mMutex };
    if (pEvent.Message == "!ads")
    {
        mAdgeOfEmpiresEndTime = std::chrono::steady_clock::now() + std::chrono::seconds(5);
        mAdgeOfEmpiresNextUnitSpawnTime = std::chrono::steady_clock::now();
        mAdgeOfEmpiresColor = glm::vec3(pEvent.UserColor.value().R, pEvent.UserColor.value().G, pEvent.UserColor.value().B) / 255.0f;
    }
}

} // AoE2DE::Overlay