#ifndef AOE2DE_OVERLAY_OVERLAY_WINDOW_HPP_INCLUDED
#define AOE2DE_OVERLAY_OVERLAY_WINDOW_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Overlay Window Interface                                                                      //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <AoE2DE/Overlay/Graphics/OpenGLWindow.hpp>
#include <AoE2DE/Overlay/Graphics/OpenGLUnitTexture.hpp>
#include <AoE2DE/Overlay/Graphics/OpenGLUnitShader.hpp>
#include <AoE2DE/Overlay/UnitClassInfo.hpp>
#include <AoE2DE/Overlay/UnitInstanceInfo.hpp>

// -------------------------------------------------------------------------------- Library Headers
#include <AoE2DE/Core/Media/SLDTexture.hpp>

#include <glm/glm.hpp>

#include <Network/Core/Dispatcher.hpp>
#include <Network/Twitch/API/EventHandler.hpp>
#include <Network/Twitch/EventSub/EventHandler.hpp>
#include <Network/Twitch/Extensions/Heat/EventHandler.hpp>
#include <Network/Twitch/IRC/EventHandler.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <array>
#include <chrono>
#include <cstdint>
#include <memory>
#include <mutex>
#include <random>
#include <vector>

namespace AoE2DE::Overlay
{

/**
 * @brief Overlay window.
 */
class OverlayWindow :
    public AoE2DE::Overlay::Graphics::OpenGLWindow,
    public Network::Twitch::API::EventHandler,
    public Network::Twitch::EventSub::EventHandler,
    public Network::Twitch::Extensions::Heat::EventHandler,
    public Network::Twitch::IRC::EventHandler
{
public:
    /**
     * @brief Constructor.
     */
    OverlayWindow();

    /**
     * @brief Destructor.
     */
    ~OverlayWindow();

    /**
     * @brief Deleted copy constructor.
     */
    OverlayWindow(const OverlayWindow&) = delete;

    /**
     * @brief Deleted move constructor.
     */
    OverlayWindow(OverlayWindow&&) = delete;

    /**
     * @brief Deleted copy-assignment operator.
     */
    OverlayWindow& operator=(const OverlayWindow&) = delete;

    /**
     * @brief Deleted move-assignment operator.
     */
    OverlayWindow& operator=(OverlayWindow&&) = delete;

private:
    /**
     * @brief Create OpenGL resources.
     *
     * This method is called after the initialization of the OpenGL context.
     *
     * @return True if the creation of OpenGL resources was successfull.
     */
    bool OnCreate() override;

    /**
     * @brief Update and render the current frame.
     *
     * @param pCurrentTime The current application time in seconds.
     * @param pElapsedTime The time since last frame in seconds.
     * @return False if an error occurred and the window should close.
     */
    bool OnUpdate(
        double pCurrentTime,
        double pElapsedTime) override;

    /**
     * @brief Destroy OpenGL resources.
     *
     * This method is called before the destruction of the OpenGL context.
     */
    void OnDestroy() override;

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Window Event Handlers                                                                     //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @brief Mouse button pressed event handler.
     *
     * @param pButton The mouse button code.
     * @param pMods The keyboard modifier codes bitmask.
     */
    void OnMouseButtonPressed(
        OpenGLWindow::MouseButton pButton,
        OpenGLWindow::KeyboardMods pMods) override;

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Twitch Event Handlers                                                                     //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    void OnEvent(
        const Network::Twitch::EventSub::Events::ChannelAdBreakBeginEvent& pEvent) override;

    void OnEvent(
        const Network::Twitch::EventSub::Events::ChannelPointsCustomRewardRedemptionAddEvent& pEvent) override;

    void OnEvent(
        const Network::Twitch::EventSub::Events::ChannelRaidEvent& pEvent) override;

    void OnEvent(
        const Network::Twitch::Extensions::Heat::Events::UserClickEvent& pEvent) override;

    void OnEvent(
        const Network::Twitch::IRC::Events::UserMessageEvent& pEvent) override;

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Twitch Event Subscriptions                                                                //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @brief Subscription to all Twitch API events.
     */
    Network::Core::Dispatcher::Subscription mTwitchAPISubscription;

    /**
     * @brief Subscription to all Twitch EventSub events.
     */
    Network::Core::Dispatcher::Subscription mTwitchEventSubSubscription;

    /**
     * @brief Subscription to all Twitch Heat events.
     */
    Network::Core::Dispatcher::Subscription mTwitchHeatSubscription;

    /**
     * @brief Subscription to all Twitch IRC events.
     */
    Network::Core::Dispatcher::Subscription mTwitchIRCSubscription;

    /**
     * @brief Mutual exclusion for data access in Twitch event handlers.
     */
    std::mutex mMutex;

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Assets                                                                                    //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    std::array<AoE2DE::Overlay::UnitClassInfo, UnitClass::UNIT_CLASS_COUNT> mUnitClassInfo;

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // OpenGL                                                                                    //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    std::unique_ptr<AoE2DE::Overlay::Graphics::OpenGLUnitShader> mUnitShader;

    /**
     * @brief Buffer of unit instance attributes.
     *
     * Instance attributes are uploaded to the GPU storage before rendering the current frame.
     * Instances are sorted by animation to enable batch rendering units sharing the same texture.
     *
     * - x (coordinate on the x axis)
     * - y (coordinate on the y axis)
     * - z (texture sampling depth)
     * - s (scaling factor)
     * - r (color channel r)
     * - g (color channel g)
     * - b (color channel b)
     */
    std::vector<float> mInstanceAttributeBuffer;

    struct UnitBatchInfo
    {
        /**
         * @brief OpenGL texture to bind to render the animation.
         */
        std::unique_ptr<AoE2DE::Overlay::Graphics::OpenGLUnitTexture> Texture;

        /**
         * @brief
         */
        std::size_t Count;

        /**
         * @brief Where the batch starts in the attribute buffer (instance count, not a byte offset).
         */
        std::size_t Offset;
    };

    static inline std::size_t GetUnitBatchIndex(
        UnitClass pUnitClass,
        UnitAnimation pUnitAnimation)
    {
        return pUnitClass * UnitAnimation::UNIT_ANIMATION_COUNT + pUnitAnimation;
    }

    /**
     * @brief
     *
     * The instance attribute buffer is split into batches of units sharing the same animation data.
     * This array describes how the buffer is split and which texture is used in which batch.
     *
     * Before updating the instance attribute buffer, the number of unit instances falling under each animation type must be counted,
     * and reported in this buffer.
     */
    std::array<UnitBatchInfo, UnitClass::UNIT_CLASS_COUNT * UnitAnimation::UNIT_ANIMATION_COUNT> mUnitBatchInfo;

    std::uint32_t mUnitVertexArrayHandle;
    std::uint32_t mUnitVertexBufferHandle;
    std::uint32_t mUnitElementBufferHandle;

    /**
     * @brief Number of instanced vertex attribute buffers in the multiple buffering pool.
     */
    static constexpr std::size_t sInstanceBufferCount{ 2ULL };

    /**
     * @brief Internal OpenGL instanced vertex attribute buffer handles.
     */
    std::uint32_t mInstanceBufferHandle[sInstanceBufferCount]{};

    /**
     * @brief Size of the instanced vertex attribute buffers, in bytes.
     */
    std::size_t mInstanceBufferSize[sInstanceBufferCount]{};

    std::size_t mCurrentInstanceBuffer{ 0ULL };

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Unit Animation (General Settings)                                                         //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @brief The simulation speed multiplier for all unit animations.
     */
    float mSimulationSpeed{ 1.0f };

    std::mt19937 mRandom;

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Unit Animation - Ad Break                                                                 //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @brief Trade units created when an ad break starts, and when users redeem more.
     */
    std::vector<AoE2DE::Overlay::UnitInstanceInfo> mTradeUnits;

    std::chrono::steady_clock::time_point mAdBreakEndTime;
    std::chrono::steady_clock::time_point mAdBreakNextUnitSpawnTime;

    glm::vec3 mAdgeOfEmpiresColor;
    std::chrono::steady_clock::time_point mAdgeOfEmpiresEndTime;
    std::chrono::steady_clock::time_point mAdgeOfEmpiresNextUnitSpawnTime;

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Unit Animation - Raid                                                                     //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @brief Raid units created when the channel is raided, and when users redeem more.
     */
    std::vector<AoE2DE::Overlay::UnitInstanceInfo> mRaidUnits;

    std::chrono::steady_clock::time_point mRaidEndTime;
    std::chrono::steady_clock::time_point mRaidNextUnitSpawnTime;

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Unit Animation - Avatars                                                                  //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @brief
     */
    struct Avatar
    {
        /**
         * @brief The target position for the avatar.
         */
        glm::vec2 TargetPosition;

        /**
         * @brief The unit instance info for the avatar.
         */
        AoE2DE::Overlay::UnitInstanceInfo Instance;

        std::chrono::steady_clock::time_point AvatarEndTime;
    };

    /**
     * @brief The stream avatar.
     *
     * It can be controlled by any viewer who does not own a personal avatar.
     */
    Avatar mStreamAvatar;

    /**
     * @brief The viewers' personal avatars.
     *
     * It can be controlled only by the viewer who redeemed it with channel points.
     */
    std::unordered_map<std::string, Avatar> mPersonalAvatars;
};

} // AoE2DE::Overlay

#endif // AOE2DE_OVERLAY_OVERLAY_WINDOW_HPP_INCLUDED