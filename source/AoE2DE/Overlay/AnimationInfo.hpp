#ifndef AOE2DE_OVERLAY_ANIMATION_INFO_HPP_INCLUDED
#define AOE2DE_OVERLAY_ANIMATION_INFO_HPP_INCLUDED

#include <cstddef>
#include <filesystem>

namespace AoE2DE::Overlay
{

/**
 * @brief Enumerate all unit animations.
 */
enum UnitAnimation
{
    Idle,
    Walk,
    Death,
    Decay,

    /**
     * @brief Number of unit animations.
     */
    UNIT_ANIMATION_COUNT
};

struct AnimationInfo
{
    std::filesystem::path Filepath;
    std::size_t NumberOfOrientations;
    std::size_t NumberOfFrames;         // Total number of frames in the animation files
    float Duration;
};

} // AoE2DE::Overlay

#endif // AOE2DE_OVERLAY_ANIMATION_INFO_HPP_INCLUDED