#ifndef AOE2DE_OVERLAY_UNIT_INSTANCE_INFO_HPP_INCLUDED
#define AOE2DE_OVERLAY_UNIT_INSTANCE_INFO_HPP_INCLUDED

#include <AoE2DE/Overlay/AnimationSampler.hpp>
#include <AoE2DE/Overlay/UnitClassInfo.hpp>

#include <glm/glm.hpp>

namespace AoE2DE::Overlay
{

struct UnitInstanceInfo
{
    UnitClass Class;
    UnitAnimation Animation;

    AnimationSampler Sampler;

    glm::vec2 Position;
    glm::vec2 Movement;
    glm::vec3 Color;
    float Scale{ 1.0f };
    float Speed{ 1.0f };
};

} // AoE2DE::Overlay

#endif // AOE2DE_OVERLAY_UNIT_INSTANCE_INFO_HPP_INCLUDED