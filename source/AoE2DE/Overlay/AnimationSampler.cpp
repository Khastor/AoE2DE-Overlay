#include <AoE2DE/Overlay/AnimationSampler.hpp>
#include <AoE2DE/Overlay/AnimationInfo.hpp>

#include <cmath>

namespace AoE2DE::Overlay
{

constexpr double TWO_PI{ 2.0 * 3.141592 };

void AnimationSampler::SetOrientation(
    float pMovementX,
    float pMovementY)
{
    if (Animation)
    {
        const double lMovementAngle{ std::round(std::fmod(std::atan2(pMovementY * std::sqrt(2.0), pMovementX) + TWO_PI, TWO_PI) / (TWO_PI / Animation->NumberOfOrientations)) };
        Orientation = (Animation->NumberOfOrientations - ((int)lMovementAngle)) % Animation->NumberOfOrientations;
    }
}

void AnimationSampler::Reset(
    AoE2DE::Overlay::AnimationInfo* pAnimationInfo)
{
    Animation = pAnimationInfo;
    Offset = 0.0f; // TODO: set offset to the animation duration if playing in reverse mode (speed < 0)
    Speed = 1.0f;
    Loop = true;
    Orientation = 0;
}

void AnimationSampler::Update(
    float pElapsedTime)
{
    if (!Animation)
    {
        return;
    }

    if (Loop)
    {
        Offset = std::fmodf(std::fmodf(Offset + Speed * pElapsedTime, Animation->Duration) + Animation->Duration, Animation->Duration);
    }
    else
    {
        Offset = std::clamp(Offset + Speed * pElapsedTime, 0.0f, Animation->Duration);
    }
}

std::size_t AnimationSampler::Sample() const
{
    if (Animation)
    {
        const std::size_t lFramesPerOrientation{ Animation->NumberOfFrames / Animation->NumberOfOrientations };
        return static_cast<std::size_t>(Orientation * lFramesPerOrientation + std::round((lFramesPerOrientation - 1) * Offset / Animation->Duration));
    }
    return 0;
}

} // AoE2DE::Overlay
