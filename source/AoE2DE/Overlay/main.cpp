///////////////////////////////////////////////////////////////////////////////////////////////////
// AoE2DE Overlay Entry Point                                                                    //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <AoE2DE/Overlay/OverlayWindow.hpp>

// -------------------------------------------------------------------------------- Library Headers
#include <Network/Core/Settings.hpp>
#include <Network/Twitch/API/Service.hpp>
#include <Network/Twitch/EventSub/Service.hpp>
#include <Network/Twitch/Extensions/Heat/Service.hpp>
#include <Network/Twitch/IRC/Service.hpp>
#include <Network/Twitch/OAuth2/Constants.hpp>
#include <Network/Twitch/OAuth2/Service.hpp>

/**
 * @brief AoE2DE Overlay application entry point.
 */
int main()
{
    // Load network settings and credentials.
    Network::Core::Settings& lSettings{ Network::Core::Settings::Instance() };
    if (!lSettings.LoadFromFile("local/settings.xml"))
    {
        return false;
    }
    lSettings.AddTwitchUserAccessScope(Network::Twitch::OAuth2::Scopes::CHANNEL_READ_ADS);
    lSettings.AddTwitchUserAccessScope(Network::Twitch::OAuth2::Scopes::CHANNEL_READ_REDEMPTIONS);
    lSettings.AddTwitchUserAccessScope(Network::Twitch::OAuth2::Scopes::CHAT_READ);
    lSettings.AddTwitchUserAccessScope(Network::Twitch::OAuth2::Scopes::CHANNEL_MODERATE);
    lSettings.AddTwitchUserAccessScope(Network::Twitch::OAuth2::Scopes::CHAT_EDIT);
    lSettings.AddTwitchUserAccessScope(Network::Twitch::OAuth2::Scopes::MODERATOR_MANAGE_ANNOUNCEMENTS);
    lSettings.AddTwitchUserAccessScope(Network::Twitch::OAuth2::Scopes::USER_READ_CHAT);
    lSettings.AddTwitchUserAccessScope(Network::Twitch::OAuth2::Scopes::USER_WRITE_CHAT);

    Network::Twitch::Extensions::Heat::Service::Initialize();
    Network::Twitch::OAuth2::Service::Initialize();
    Network::Twitch::IRC::Service::Initialize();
    Network::Twitch::API::Service::Initialize();
    Network::Twitch::EventSub::Service::Initialize();

    AoE2DE::Overlay::OverlayWindow lOverlayWindow;
    lOverlayWindow.Show();
}