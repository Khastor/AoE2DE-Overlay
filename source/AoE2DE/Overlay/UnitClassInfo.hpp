#ifndef AOE2DE_OVERLAY_UNIT_CLASS_INFO_HPP_INCLUDED
#define AOE2DE_OVERLAY_UNIT_CLASS_INFO_HPP_INCLUDED

#include <AoE2DE/Overlay/AnimationInfo.hpp>

namespace AoE2DE::Overlay
{

/**
 * @brief Enumerate all unit classes.
 */
enum UnitClass
{
    WarElephant,
    TradeCart,
    EliteEagleWarrior,
    ThrowingAxeman,

    /**
     * @brief Number of unit classes.
     */
    UNIT_CLASS_COUNT
};

struct UnitClassInfo
{
    AoE2DE::Overlay::AnimationInfo AttackAnimationInfo;
    AoE2DE::Overlay::AnimationInfo DeathAnimationInfo;
    AoE2DE::Overlay::AnimationInfo DecayAnimationInfo;
    AoE2DE::Overlay::AnimationInfo IdleAnimationInfo;
    AoE2DE::Overlay::AnimationInfo RunAnimationInfo;
    AoE2DE::Overlay::AnimationInfo WalkAnimationInfo;
    float WalkSpeed;
    float RunSpeed;
};

} // AoE2DE::Overlay

#endif // AOE2DE_OVERLAY_UNIT_CLASS_INFO_HPP_INCLUDED