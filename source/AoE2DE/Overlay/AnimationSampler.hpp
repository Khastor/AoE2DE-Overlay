#ifndef AOE2DE_OVERLAY_ANIMATION_SAMPLER_HPP_INCLUDED
#define AOE2DE_OVERLAY_ANIMATION_SAMPLER_HPP_INCLUDED

#include <cstddef>

namespace AoE2DE::Overlay
{

struct AnimationInfo;

struct AnimationSampler
{
    AnimationInfo* Animation;
    bool Loop;
    float Speed;
    float Offset;
    std::size_t Orientation;

    void SetOrientation(
        float pMovementX,
        float pMovementY);

    void Reset(
        AoE2DE::Overlay::AnimationInfo* pAnimationInfo);

    void Update(
        float pElapsedTime);

    std::size_t Sample() const;
};


} // AoE2DE::Overlay

#endif // AOE2DE_OVERLAY_ANIMATION_SAMPLER_HPP_INCLUDED