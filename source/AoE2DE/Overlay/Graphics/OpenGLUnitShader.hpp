#ifndef AOE2DE_OVERLAY_GRAPHICS_OPENGL_UNIT_SHADER_HPP_INCLUDED
#define AOE2DE_OVERLAY_GRAPHICS_OPENGL_UNIT_SHADER_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// OpenGL Unit Shader Interface                                                                  //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <AoE2DE/Overlay/Graphics/OpenGLShader.hpp>

// -------------------------------------------------------------------------------- Library Headers
#include <glm/glm.hpp>

namespace AoE2DE::Overlay::Graphics
{

/**
 * @brief OpenGL shader.
 */
class OpenGLUnitShader : public OpenGLShader
{
public:
    /**
     * @brief Constructor.
     *
     * Upon failure, the resulting OpenGL shader is null.
     */
    OpenGLUnitShader();

    /**
     * @brief Deleted copy constructor.
     */
    OpenGLUnitShader(const OpenGLUnitShader&) = delete;

    /**
     * @brief Deleted move constructor.
     */
    OpenGLUnitShader(OpenGLUnitShader&&) = delete;

    /**
     * @brief Deleted copy-assignment operator.
     */
    OpenGLUnitShader& operator=(const OpenGLUnitShader&) = delete;

    /**
     * @brief Deleted move-assignment operator.
     */
    OpenGLUnitShader& operator=(OpenGLUnitShader&&) = delete;

    /**
     * @brief Set the texture size.
     *
     * @param pTextureSize The texture size.
     */
    void SetTextureSize(
        const glm::vec2& pTextureSize);

    /**
     * @brief Set the texture hotspot coordinates.
     *
     * @param pTextureHotspot The texture hotspot coordinates.
     */
    void SetTextureHotspot(
        const glm::vec2& pTextureHotspot);
};

} // AoE2DE::Overlay::Graphics

#endif // AOE2DE_OVERLAY_GRAPHICS_OPENGL_UNIT_SHADER_HPP_INCLUDED