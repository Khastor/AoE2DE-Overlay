#ifndef AOE2DE_OVERLAY_GRAPHICS_OPENGL_WINDOW_HPP_INCLUDED
#define AOE2DE_OVERLAY_GRAPHICS_OPENGL_WINDOW_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// OpenGL Window Interface                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <cstdint>
#include <string>

struct GLFWwindow;

namespace AoE2DE::Overlay::Graphics
{

/**
 * @brief OpenGL window.
 */
class OpenGLWindow
{
public:
    /**
     * @brief Mouse button codes.
     */
    enum class MouseButton : std::int32_t
    {
        BUTTON_1,
        BUTTON_2,
        BUTTON_3,
        BUTTON_4,
        BUTTON_5,
        BUTTON_6,
        BUTTON_7,
        BUTTON_8,
        BUTTON_LEFT = BUTTON_1,
        BUTTON_RIGHT = BUTTON_2,
        BUTTON_MIDDLE = BUTTON_3
    };

    /**
     * @brief Keyboard key codes.
     */
    enum class KeyboardKey : std::int32_t
    {
        KEY_UNKNOWN,
        KEY_SPACE,
        KEY_APOSTROPHE,
        KEY_COMMA,
        KEY_MINUS,
        KEY_PERIOD,
        KEY_SLASH,
        KEY_0,
        KEY_1,
        KEY_2,
        KEY_3,
        KEY_4,
        KEY_5,
        KEY_6,
        KEY_7,
        KEY_8,
        KEY_9,
        KEY_SEMICOLON,
        KEY_EQUAL,
        KEY_A,
        KEY_B,
        KEY_C,
        KEY_D,
        KEY_E,
        KEY_F,
        KEY_G,
        KEY_H,
        KEY_I,
        KEY_J,
        KEY_K,
        KEY_L,
        KEY_M,
        KEY_N,
        KEY_O,
        KEY_P,
        KEY_Q,
        KEY_R,
        KEY_S,
        KEY_T,
        KEY_U,
        KEY_V,
        KEY_W,
        KEY_X,
        KEY_Y,
        KEY_Z,
        KEY_LEFT_BRACKET,
        KEY_BACKSLASH,
        KEY_RIGHT_BRACKET,
        KEY_GRAVE_ACCENT,
        KEY_WORLD_1,
        KEY_WORLD_2,
        KEY_ESCAPE,
        KEY_ENTER,
        KEY_TAB,
        KEY_BACKSPACE,
        KEY_INSERT,
        KEY_DELETE,
        KEY_RIGHT,
        KEY_LEFT,
        KEY_DOWN,
        KEY_UP,
        KEY_PAGE_UP,
        KEY_PAGE_DOWN,
        KEY_HOME,
        KEY_END,
        KEY_CAPS_LOCK,
        KEY_SCROLL_LOCK,
        KEY_NUM_LOCK,
        KEY_PRINT_SCREEN,
        KEY_PAUSE,
        KEY_F1,
        KEY_F2,
        KEY_F3,
        KEY_F4,
        KEY_F5,
        KEY_F6,
        KEY_F7,
        KEY_F8,
        KEY_F9,
        KEY_F10,
        KEY_F11,
        KEY_F12,
        KEY_F13,
        KEY_F14,
        KEY_F15,
        KEY_F16,
        KEY_F17,
        KEY_F18,
        KEY_F19,
        KEY_F20,
        KEY_F21,
        KEY_F22,
        KEY_F23,
        KEY_F24,
        KEY_F25,
        KEY_KP_0,
        KEY_KP_1,
        KEY_KP_2,
        KEY_KP_3,
        KEY_KP_4,
        KEY_KP_5,
        KEY_KP_6,
        KEY_KP_7,
        KEY_KP_8,
        KEY_KP_9,
        KEY_KP_DECIMAL,
        KEY_KP_DIVIDE,
        KEY_KP_MULTIPLY,
        KEY_KP_SUBTRACT,
        KEY_KP_ADD,
        KEY_KP_ENTER,
        KEY_KP_EQUAL,
        KEY_LEFT_SHIFT,
        KEY_LEFT_CONTROL,
        KEY_LEFT_ALT,
        KEY_LEFT_SUPER,
        KEY_RIGHT_SHIFT,
        KEY_RIGHT_CONTROL,
        KEY_RIGHT_ALT,
        KEY_RIGHT_SUPER,
        KEY_MENU,
        KEY_LAST = KEY_MENU
    };

    /**
     * @brief Keyboard modifier codes.
     */
    enum class KeyboardMod : std::int32_t
    {
        MOD_SHIFT = 1 << 0,
        MOD_CONTROL = 1 << 1,
        MOD_ALT = 1 << 2,
        MOD_SUPER = 1 << 3,
        MOD_CAPS_LOCK = 1 << 4,
        MOD_NUM_LOCK = 1 << 5
    };

    /**
     * @brief Keyboard modifier codes bitmask.
     */
    using KeyboardMods = std::int32_t;

    /**
     * @brief Constructor.
     *
     * The OpenGL context will not be created until the window is shown.
     * Derived classes must not call OpenGL functions from the constructor.
     *
     * @param pOpenGLVersionMajor The OpenGL major version number.
     * @param pOpenGLVersionMinor The OpenGL minor version number.
     * @param pFramebufferSamples The number of MSAA samples.
     * @param pTitle The window title.
     */
    OpenGLWindow(
        std::uint8_t pOpenGLVersionMajor,
        std::uint8_t pOpenGLVersionMinor,
        std::uint8_t pFramebufferSamples,
        const std::string& pTitle);

    /**
     * @brief Destructor.
     */
    virtual ~OpenGLWindow() = default;

    /**
     * @brief Deleted copy constructor. 
     */
    OpenGLWindow(const OpenGLWindow&) = delete;

    /**
     * @brief Deleted move constructor. 
     */
    OpenGLWindow(OpenGLWindow&&) = delete;

    /**
     * @brief Deleted copy-assignment operator.
     */
    OpenGLWindow& operator=(const OpenGLWindow&) = delete;

    /**
     * @brief Deleted move-assignment operator.
     */
    OpenGLWindow& operator=(OpenGLWindow&&) = delete;

    /**
     * @brief Show the window.
     *
     * This method is blocking and will return when the window is closed.
     */
    void Show();

protected:
    /**
     * @brief Create OpenGL resources.
     *
     * This method is called after the initialization of the OpenGL context.
     *
     * @return True if the creation of OpenGL resources was successfull.
     */
    virtual bool OnCreate() = 0;

    /**
     * @brief Update and render the current frame.
     *
     * @param pCurrentTime The current application time in seconds.
     * @param pElapsedTime The time since last frame in seconds.
     * @return False if an error occurred and the window should close.
     */
    virtual bool OnUpdate(
        double pCurrentTime,
        double pElapsedTime) = 0;

    /**
     * @brief Destroy OpenGL resources.
     *
     * This method is called before the destruction of the OpenGL context.
     */
    virtual void OnDestroy() = 0;

    /**
     * @brief Framebuffer resized event handler.
     *
     * The default implementation updates the OpenGL viewport.
     *
     * @param pWidth Framebuffer width in pixels.
     * @param pHeight pFramebuffer height in pixels.
     */
    virtual void OnFramebufferResized(
        int pWidth,
        int pHeight);

    /**
     * @brief Keyboard key pressed event handler.
     *
     * The default implementation has no effect.
     *
     * @param pKey The keyboard key code.
     * @param pMods The keyboard modifier codes bitmask.
     */
    virtual void OnKeyPressed(
        OpenGLWindow::KeyboardKey pKey,
        OpenGLWindow::KeyboardMods pMods);

    /**
     * @brief Keyboard key released event handler.
     *
     * The default implementation has no effect.
     *
     * @param pKey The keyboard key code.
     * @param pMods The keyboard modifier codes bitmask.
     */
    virtual void OnKeyReleased(
        OpenGLWindow::KeyboardKey pKey,
        OpenGLWindow::KeyboardMods pMods);

    /**
     * @brief Keyboard key repeated event handler.
     *
     * The default implementation has no effect.
     *
     * @param pKey The keyboard key code.
     * @param pMods The keyboard modifier codes bitmask.
     */
    virtual void OnKeyRepeated(
        OpenGLWindow::KeyboardKey pKey,
        OpenGLWindow::KeyboardMods pMods);

    /**
     * @brief Mouse button pressed event handler.
     *
     * The default implementation has no effect.
     *
     * @param pButton The mouse button code.
     * @param pMods The keyboard modifier codes bitmask.
     */
    virtual void OnMouseButtonPressed(
        OpenGLWindow::MouseButton pButton,
        OpenGLWindow::KeyboardMods pMods);

    /**
     * @brief Mouse button released event handler.
     *
     * The default implementation no effect.
     *
     * @param pButton The mouse button code.
     * @param pMods The keyboard modifier codes bitmask.
     */
    virtual void OnMouseButtonReleased(
        OpenGLWindow::MouseButton pButton,
        OpenGLWindow::KeyboardMods pMods);

    /**
     * @brief Mouse cursor moved event handler.
     *
     * The default implementation no effect.
     *
     * @param pCursorX The mouse cursor position x.
     * @param pCursorY The mouse cursor position y.
     */
    virtual void OnMouseMoved(
        double pCursorX,
        double pCursorY);

    /**
     * @brief Mouse scrolled event handler.
     *
     * The default implementation no effect.
     *
     * @param pOffsetX The mouse scroll offset x.
     * @param pOffsetY The mouse scroll offset y.
     */
    virtual void OnMouseScrolled(
        double pOffsetX,
        double pOffsetY);

    /**
     * @brief File path drop event handler.
     *
     * The default implementation no effect.
     *
     * @param pPaths The array of file paths encoded in UTF-8.
     * @param pCount The size of the array of file paths.
     */
    virtual void OnPathsDropped(
        const char** pPaths,
        std::uint32_t pCount);

    /**
     * @brief Test whether the window is iconified.
     *
     * @return True if the window is iconified
     */
    bool IsIconified() const;

    /**
     * @brief Test whether the mouse cursor is visible.
     *
     * @return True if the mouse cursor is visible.
     */
    bool IsCursorEnabled() const;

    /**
     * @brief Set the mouse cursor visibility.
     *
     * @param pEnabled True to set the mouse cursor visible.
     */
    void SetCursorEnabled(
        bool pEnabled);

    /**
     * @brief Test whether the window framerate is fixed.
     *
     * @return True if the window framerate is fixed.
     */
    bool IsFramerateFixed() const;

    /**
     * @brief Set whether the window framerate is fixed.
     *
     * @param pFixed True to set a fixed window framerate.
     */
    void SetFramerateFixed(
        bool pFixed);

    /**
     * @brief Get the framebuffer width.
     *
     * @return The framebuffer width, in pixels.
     */
    int GetFramebufferWidth() const;

    /**
     * @brief Get the framebuffer height.
     *
     * @return The framebuffer height, in pixels.
     */
    int GetFramebufferHeight() const;

    /**
     * @brief Get the cursor coordinate on the x axis.
     *
     * @return The cursor coordinate on the x axis relative to the content area, in screen coordinates.
     */
    double GetCursorX() const;

    /**
     * @brief Get the cursor coordinate on the y axis.
     *
     * @return The cursor coordinate on the y axis relative to the content area, in screen coordinates.
     */
    double GetCursorY() const;

    /**
     * @brief Test the state of a keyboard key.
     *
     * @param pKey The keyboard key whose state to test.
     * @return True if the keyboard key is currently pressed.
     */
    bool IsKeyPressed(
        OpenGLWindow::KeyboardKey pKey) const;

    /**
     * @brief Test the state of a mouse button.
     *
     * @param pButton The mouse button whose state to test.
     * @return True if the mouse button is currently pressed.
     */
    bool IsMouseButtonPressed(
        OpenGLWindow::MouseButton pButton) const;

    /**
     * @brief Test if a keyboard modifier is part of the keyboard modifiers bitmask.
     *
     * @param pMods The keyboard modifier bitmask.
     * @param pMod The keyboard modifier to test.
     * @return True if pMod is part of pMods.
     */
    static bool HasKeyboardMod(
        OpenGLWindow::KeyboardMods pMods,
        OpenGLWindow::KeyboardMod pMod);

    /**
     * @brief Notify the user of an event that requires attention to the window.
     */
    void UserNotification() const;

    /**
     * @brief Set the system clipboard string.
     *
     * @param pMessage The UTF-8 encoded string.
     */
    void SetClipboardString(
        const std::string& pString);

    /**
     * @brief Show the window settings in the immediate mode GUI layer.
     */
    void ImGuiShowWindowSettings();

private:
    /**
     * @brief The OpenGL major version number.
     */
    const std::uint8_t mOpenGLVersionMajor;

    /**
     * @brief The OpenGL minor version number.
     */
    const std::uint8_t mOpenGLVersionMinor;

    /**
     * @brief The number of samples used for multisample anti-aliasing.
     */
    const std::uint8_t mFramebufferSamples;

    /**
     * @brief The window title.
     */
    const std::string mTitle;

    /**
     * @brief The underlying window implementation.
     */
    GLFWwindow* mWindow{ nullptr };

    /**
     * @brief The window iconified state.
     */
    bool mWindowIconified{ false };

    /**
     * @brief The window framerate mode.
     */
    bool mWindowFramerateFixed{ false };

    /**
     * @brief The framebuffer width, in pixels.
     */
    int mWindowFramebufferWidth{ 0 };

    /**
     * @brief The framebuffer height, in pixels.
     */
    int mWindowFramebufferHeight{ 0 };

    /**
     * @brief The position of the mouse cursor on the x axis.
     */
    double mWindowCursorX{ 0 };

    /**
     * @brief The position of the mouse cursor on the y axis.
     */
    double mWindowCursorY{ 0 };
};

} // AoE2DE::Overlay::Graphics

#endif // AOE2DE_OVERLAY_GRAPHICS_OPENGL_WINDOW_HPP_INCLUDED