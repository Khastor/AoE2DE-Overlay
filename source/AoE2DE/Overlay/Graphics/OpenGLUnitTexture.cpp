///////////////////////////////////////////////////////////////////////////////////////////////////
// OpenGL Unit Texture Implementation                                                            //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <AoE2DE/Overlay/Graphics/OpenGLUnitTexture.hpp>

// -------------------------------------------------------------------------------- Library Headers
#include <AoE2DE/Core/Media/SLDTexture.hpp>

#include <glad/gl.h>

// --------------------------------------------------------------------------------- System Headers
#include <algorithm>
#include <cstddef>
#include <limits>

namespace AoE2DE::Overlay::Graphics
{

OpenGLUnitTexture::OpenGLUnitTexture(
    const AoE2DE::Core::Media::SLDTexture& pSLDTexture)
{
    const std::size_t lFrameCount{ pSLDTexture.GetFrameCount() };
    if (!lFrameCount)
    {
        return;
    }

    glCreateTextures(GL_TEXTURE_2D_ARRAY, 3, mHandle);

    // Compute the texture array bounding box top-left and bottom-right corners relative to the hotspot.
    std::int32_t lBoundingBoxMinX{ std::numeric_limits<std::int32_t>::max() };
    std::int32_t lBoundingBoxMinY{ std::numeric_limits<std::int32_t>::max() };
    std::int32_t lBoundingBoxMaxX{ std::numeric_limits<std::int32_t>::min() };
    std::int32_t lBoundingBoxMaxY{ std::numeric_limits<std::int32_t>::min() };
    for (std::size_t lFrame{}; lFrame < lFrameCount; lFrame++)
    {
        // The color and player mask layers have the same offset and dimensions.
        const auto lColorFrameInfo{ pSLDTexture.GetLayer(lFrame, AoE2DE::Core::Media::SLDTexture::LayerType::Color) };
        lBoundingBoxMinX = std::min(lBoundingBoxMinX, static_cast<std::int32_t>(lColorFrameInfo.OffsetX) - lColorFrameInfo.HotspotX);
        lBoundingBoxMinY = std::min(lBoundingBoxMinY, static_cast<std::int32_t>(lColorFrameInfo.OffsetY) - lColorFrameInfo.HotspotY);
        lBoundingBoxMaxX = std::max(lBoundingBoxMaxX, static_cast<std::int32_t>(lColorFrameInfo.OffsetX + lColorFrameInfo.Width) - lColorFrameInfo.HotspotX);
        lBoundingBoxMaxY = std::max(lBoundingBoxMaxY, static_cast<std::int32_t>(lColorFrameInfo.OffsetY + lColorFrameInfo.Height) - lColorFrameInfo.HotspotY);

        const auto lShadowFrameInfo{ pSLDTexture.GetLayer(lFrame, AoE2DE::Core::Media::SLDTexture::LayerType::Shadow) };
        lBoundingBoxMinX = std::min(lBoundingBoxMinX, static_cast<std::int32_t>(lShadowFrameInfo.OffsetX) - lShadowFrameInfo.HotspotX);
        lBoundingBoxMinY = std::min(lBoundingBoxMinY, static_cast<std::int32_t>(lShadowFrameInfo.OffsetY) - lShadowFrameInfo.HotspotY);
        lBoundingBoxMaxX = std::max(lBoundingBoxMaxX, static_cast<std::int32_t>(lShadowFrameInfo.OffsetX + lShadowFrameInfo.Width) - lShadowFrameInfo.HotspotX);
        lBoundingBoxMaxY = std::max(lBoundingBoxMaxY, static_cast<std::int32_t>(lShadowFrameInfo.OffsetY + lShadowFrameInfo.Height) - lShadowFrameInfo.HotspotY);
    }
    // The texture arrays have the same dimensions and are all aligned on the same hotspot coordinates.
    mWidth = static_cast<std::uint32_t>(lBoundingBoxMaxX - lBoundingBoxMinX);
    mHeight = static_cast<std::uint32_t>(lBoundingBoxMaxY - lBoundingBoxMinY);
    mHotspotX = -lBoundingBoxMinX;
    mHotspotY = -lBoundingBoxMinY;

    // Fill the color texture array.
    glTextureStorage3D(mHandle[0], 1, GL_RGBA8, mWidth, mHeight, lFrameCount);
    glTextureParameteri(mHandle[0], GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTextureParameteri(mHandle[0], GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTextureParameteri(mHandle[0], GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTextureParameteri(mHandle[0], GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    for (std::size_t lFrame{}; lFrame < lFrameCount; lFrame++)
    {
        const auto lFrameInfo{ pSLDTexture.GetLayer(lFrame, AoE2DE::Core::Media::SLDTexture::LayerType::Color) };
        glClearTexSubImage(mHandle[0], 0, 0, 0, lFrame, mWidth, mHeight, 1, GL_RGBA, GL_UNSIGNED_BYTE, 0);
        if (lFrameInfo.Texels)
        {
            const std::int32_t lOffsetX{ static_cast<std::int32_t>(lFrameInfo.OffsetX) - lFrameInfo.HotspotX - lBoundingBoxMinX };
            const std::int32_t lOffsetY{ static_cast<std::int32_t>(lFrameInfo.OffsetY) - lFrameInfo.HotspotY - lBoundingBoxMinY };
            glTextureSubImage3D(mHandle[0], 0, lOffsetX, lOffsetY, lFrame, lFrameInfo.Width, lFrameInfo.Height, 1, GL_RGBA, GL_UNSIGNED_BYTE, lFrameInfo.Texels);
        }

    }

    // Fill the player mask texture array.
    glTextureStorage3D(mHandle[1], 1, GL_R8, mWidth, mHeight, lFrameCount);
    glTextureParameteri(mHandle[1], GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTextureParameteri(mHandle[1], GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTextureParameteri(mHandle[1], GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTextureParameteri(mHandle[1], GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    for (std::size_t lFrame{}; lFrame < lFrameCount; lFrame++)
    {
        const auto lFrameInfo{ pSLDTexture.GetLayer(lFrame, AoE2DE::Core::Media::SLDTexture::LayerType::PlayerColorMask) };
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glClearTexSubImage(mHandle[1], 0, 0, 0, lFrame, mWidth, mHeight, 1, GL_RED, GL_UNSIGNED_BYTE, 0);
        if (lFrameInfo.Texels)
        {
            const std::int32_t lOffsetX{ static_cast<std::int32_t>(lFrameInfo.OffsetX) - lFrameInfo.HotspotX - lBoundingBoxMinX };
            const std::int32_t lOffsetY{ static_cast<std::int32_t>(lFrameInfo.OffsetY) - lFrameInfo.HotspotY - lBoundingBoxMinY };
            glTextureSubImage3D(mHandle[1], 0, lOffsetX, lOffsetY, lFrame, lFrameInfo.Width, lFrameInfo.Height, 1, GL_RED, GL_UNSIGNED_BYTE, lFrameInfo.Texels);
        }
        glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
    }

    // Fill the shadow texture array.
    glTextureStorage3D(mHandle[2], 2, GL_R8, mWidth, mHeight, lFrameCount);
    glTextureParameteri(mHandle[2], GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTextureParameteri(mHandle[2], GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTextureParameteri(mHandle[2], GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTextureParameteri(mHandle[2], GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    for (std::size_t lFrame{}; lFrame < lFrameCount; lFrame++)
    {
        const auto lFrameInfo{ pSLDTexture.GetLayer(lFrame, AoE2DE::Core::Media::SLDTexture::LayerType::Shadow) };
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glClearTexSubImage(mHandle[2], 0, 0, 0, lFrame, mWidth, mHeight, 1, GL_RED, GL_UNSIGNED_BYTE, 0);
        if (lFrameInfo.Texels)
        {
            const std::int32_t lOffsetX{ static_cast<std::int32_t>(lFrameInfo.OffsetX) - lFrameInfo.HotspotX - lBoundingBoxMinX };
            const std::int32_t lOffsetY{ static_cast<std::int32_t>(lFrameInfo.OffsetY) - lFrameInfo.HotspotY - lBoundingBoxMinY };
            glTextureSubImage3D(mHandle[2], 0, lOffsetX, lOffsetY, lFrame, lFrameInfo.Width, lFrameInfo.Height, 1, GL_RED, GL_UNSIGNED_BYTE, lFrameInfo.Texels);
        }
        glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
    }
}

OpenGLUnitTexture::~OpenGLUnitTexture()
{
    glDeleteTextures(3, mHandle);
}

bool OpenGLUnitTexture::IsNull() const
{
    return !(mHandle[0] && mHandle[1] && mHandle[2]);
}

void OpenGLUnitTexture::Bind()
{
    glBindTextureUnit(0, mHandle[0]);
    glBindTextureUnit(1, mHandle[1]);
    glBindTextureUnit(2, mHandle[2]);
}

std::uint32_t OpenGLUnitTexture::GetWidth() const
{
    return mWidth;
}

std::uint32_t OpenGLUnitTexture::GetHeight() const
{
    return mHeight;
}

std::int32_t OpenGLUnitTexture::GetHotspotX() const
{
    return mHotspotX;
}

std::int32_t OpenGLUnitTexture::GetHotspotY() const
{
    return mHotspotY;
}

} // AoE2DE::Overlay::Graphics