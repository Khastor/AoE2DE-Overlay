#ifndef AOE2DE_OVERLAY_GRAPHICS_OPENGL_UNIT_TEXTURE_HPP_INCLUDED
#define AOE2DE_OVERLAY_GRAPHICS_OPENGL_UNIT_TEXTURE_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// OpenGL Unit Texture Interface                                                                 //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <cstdint>

namespace AoE2DE::Core::Media
{

class SLDTexture;

} // AoE2DE::Core::Media

namespace AoE2DE::Overlay::Graphics
{

/**
 * @brief OpenGL 2D texture arrays for a unit.
 */
class OpenGLUnitTexture
{
public:
    /**
     * @brief Constructor.
     *
     * Upon failure, the resulting OpenGL unit texture is null.
     *
     * @param pSLDTexture The source SLD texture.
     */
    OpenGLUnitTexture(
        const AoE2DE::Core::Media::SLDTexture& pSLDTexture);

    /**
     * @brief Destructor.
     */
    virtual ~OpenGLUnitTexture();

    /**
     * @brief Deleted copy constructor.
     */
    OpenGLUnitTexture(const OpenGLUnitTexture&) = delete;

    /**
     * @brief Deleted move constructor.
     */
    OpenGLUnitTexture(OpenGLUnitTexture&&) = delete;

    /**
     * @brief Deleted copy-assignment operator.
     */
    OpenGLUnitTexture& operator=(const OpenGLUnitTexture&) = delete;

    /**
     * @brief Deleted move-assignment operator.
     */
    OpenGLUnitTexture& operator=(OpenGLUnitTexture&&) = delete;

    /**
     * @brief Test if the OpenGL unit texture is null.
     *
     * The OpenGL unit texture is null if it failed construction.
     *
     * @return True if the OpenGL unit texture is null.
     */
    bool IsNull() const;

    /**
     * @brief Bind the OpenGL 2D texture arrays.
     *
     * The OpenGL 2D texture array containing color data is bound to texture unit 0.
     * The OpenGL 2D texture array containing player mask data is bound to texture unit 1.
     * The OpenGL 2D texture array containing shadow data is bound to texture unit 2.
     */
    void Bind();

    /**
     * @brief Get the texture width.
     *
     * All the texture frames have the same width.
     *
     * @return The texture width.
     */
    std::uint32_t GetWidth() const;

    /**
     * @brief Get the texture height.
     *
     * All the texture frames have the same height.
     *
     * @return The texture height.
     */
    std::uint32_t GetHeight() const;

    /**
     * @brief Get the texture hotspot coordinate on the x axis.
     *
     * Hotspot coordinates are relative to the top-left corner of the texture.
     *
     * @return The texture hotspot coordinate on the x axis.
     */
    std::int32_t GetHotspotX() const;

    /**
     * @brief Get the texture hotspot coordinate on the y axis.
     *
     * Hotspot coordinates are relative to the top-left corner of the texture.
     *
     * @return The texture hotspot coordinate on the y axis.
     */
    std::int32_t GetHotspotY() const;

private:
    /**
     * @brief Internal OpenGL texture array handles.
     */
    std::uint32_t mHandle[3]{ 0, 0, 0 };

    /**
     * @brief Texture array width.
     */
    std::uint32_t mWidth{ 0 };

    /**
     * @brief Texture array height.
     */
    std::uint32_t mHeight{ 0 };

    /**
     * @brief Texture hotspot coordinate on the x axis.
     */
    std::int32_t mHotspotX{ 0 };

    /**
     * @brief Texture hotspot coordinate on the y axis.
     */
    std::int32_t mHotspotY{ 0 };
};

} // AoE2DE::Overlay::Graphics

#endif // AOE2DE_OVERLAY_GRAPHICS_OPENGL_UNIT_TEXTURE_HPP_INCLUDED