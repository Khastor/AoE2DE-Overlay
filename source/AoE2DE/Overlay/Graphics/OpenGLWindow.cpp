///////////////////////////////////////////////////////////////////////////////////////////////////
// OpenGL Window Implementation                                                                  //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <AoE2DE/Overlay/Graphics/OpenGLWindow.hpp>

// -------------------------------------------------------------------------------- Library Headers
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#include <glad/gl.h>
#include <GLFW/glfw3.h>

// --------------------------------------------------------------------------------- System Headers
#include <format>
#include <iostream>

namespace AoE2DE::Overlay::Graphics
{

namespace
{

OpenGLWindow::KeyboardKey GLFWCodeToKeyboardKey(
    std::int32_t pCode);

OpenGLWindow::KeyboardMods GLFWCodeToKeyboardMods(
    std::int32_t pCode);

OpenGLWindow::MouseButton GLFWCodeToMouseButton(
    std::int32_t pCode);

std::int32_t KeyboardKeyToGLFWCode(
    OpenGLWindow::KeyboardKey pCode);

std::int32_t MouseButtonToGLFWCode(
    OpenGLWindow::MouseButton pCode);

} // anonymous namespace

void GLAPIENTRY GLMessageCallback(
    GLenum pSource,
    GLenum pType,
    GLuint pId,
    GLenum pSeverity,
    GLsizei pLength,
    const GLchar* pMessage,
    const void* pUser)
{
    std::cerr << "[OpenGL] " << pMessage << std::endl;
}

void GLFWMessageCallback(
    int pCode,
    const char* pMessage)
{
    std::cerr << "[GLFW] " << pMessage << std::endl;
}

OpenGLWindow::OpenGLWindow(
    std::uint8_t pOpenGLVersionMajor,
    std::uint8_t pOpenGLVersionMinor,
    std::uint8_t pFramebufferSamples,
    const std::string& pTitle) :
    mOpenGLVersionMajor(pOpenGLVersionMajor),
    mOpenGLVersionMinor(pOpenGLVersionMinor),
    mFramebufferSamples(pFramebufferSamples),
    mTitle(pTitle)
{}

void OpenGLWindow::Show()
{
    glfwSetErrorCallback(GLFWMessageCallback);
    if (!glfwInit())
    {
        std::cerr << "[GLFW] Failed to initialize." << std::endl;
        return;
    }

    if (GLFWmonitor * lMonitor{ glfwGetPrimaryMonitor() })
    {
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, mOpenGLVersionMajor);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, mOpenGLVersionMinor);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
        glfwWindowHint(GLFW_SAMPLES, mFramebufferSamples);
        glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);

        const GLFWvidmode* lVideoMode{ glfwGetVideoMode(lMonitor) };
        mWindow = glfwCreateWindow(lVideoMode->width, lVideoMode->height, mTitle.c_str(), nullptr, nullptr);
        if (!mWindow)
        {
            std::cerr << "[GLFW] Failed to create window." << std::endl;
            glfwTerminate();
            return;
        }
        int lMonitorPositionX, lMonitorPositionY;
        glfwGetMonitorPos(lMonitor, &lMonitorPositionX, &lMonitorPositionY);
        glfwSetWindowPos(mWindow, lMonitorPositionX, lMonitorPositionY);
        glfwShowWindow(mWindow);
    }
    else
    {
        std::cerr << "[GLFW] Failed to detect monitor." << std::endl;
        glfwTerminate();
        return;
    }

    glfwMakeContextCurrent(mWindow);
    if (!gladLoadGL((GLADloadfunc)glfwGetProcAddress))
    {
        std::cerr << "[GLAD] Failed to load OpenGL." << std::endl;
        glfwTerminate();
        return;
    }

    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(GLMessageCallback, nullptr);

    SetFramerateFixed(true);
    glfwGetFramebufferSize(mWindow, &mWindowFramebufferWidth, &mWindowFramebufferHeight);
    glfwGetCursorPos(mWindow, &mWindowCursorX, &mWindowCursorY);
    glfwSetInputMode(mWindow, GLFW_LOCK_KEY_MODS, GLFW_TRUE);
    glViewport(0, 0, mWindowFramebufferWidth, mWindowFramebufferHeight);

    glfwSetWindowUserPointer(mWindow, this);

    glfwSetFramebufferSizeCallback(mWindow,
        [](GLFWwindow* pWindow, int pWidth, int pHeight)
        {
            OpenGLWindow* lOpenGLWindow{ static_cast<OpenGLWindow*>(glfwGetWindowUserPointer(pWindow)) };
            lOpenGLWindow->mWindowFramebufferWidth = pWidth;
            lOpenGLWindow->mWindowFramebufferHeight = pHeight;
            lOpenGLWindow->OnFramebufferResized(pWidth, pHeight);
        });

    glfwSetWindowRefreshCallback(mWindow,
        [](GLFWwindow* pWindow)
        {
            OpenGLWindow* lOpenGLWindow{ static_cast<OpenGLWindow*>(glfwGetWindowUserPointer(pWindow)) };
            if (!lOpenGLWindow->OnUpdate(glfwGetTime(), 0.0))
            {
                glfwSetWindowShouldClose(pWindow, GLFW_TRUE);
            }
        });

    glfwSetWindowIconifyCallback(mWindow,
        [](GLFWwindow* pWindow, int pIconified)
        {
            OpenGLWindow* lOpenGLWindow{ static_cast<OpenGLWindow*>(glfwGetWindowUserPointer(pWindow)) };
            lOpenGLWindow->mWindowIconified = pIconified;
        });

    glfwSetCursorPosCallback(mWindow,
        [](GLFWwindow* pWindow, double pCursorX, double pCursorY)
        {
            if (!ImGui::GetIO().WantCaptureMouse)
            {
                OpenGLWindow* lOpenGLWindow{ static_cast<OpenGLWindow*>(glfwGetWindowUserPointer(pWindow)) };
                lOpenGLWindow->OnMouseMoved(pCursorX, pCursorY);
            }
        });

    glfwSetScrollCallback(mWindow,
        [](GLFWwindow* pWindow, double pOffsetX, double pOffsetY)
        {
            if (!ImGui::GetIO().WantCaptureMouse)
            {
                OpenGLWindow* lOpenGLWindow{ static_cast<OpenGLWindow*>(glfwGetWindowUserPointer(pWindow)) };
                lOpenGLWindow->OnMouseScrolled(pOffsetX, pOffsetY);
            }
        });

    glfwSetMouseButtonCallback(mWindow,
        [](GLFWwindow* pWindow, int pButton, int pAction, int pMods)
        {
            if (!ImGui::GetIO().WantCaptureMouse)
            {
                OpenGLWindow* lOpenGLWindow{ static_cast<OpenGLWindow*>(glfwGetWindowUserPointer(pWindow)) };
                switch (pAction)
                {
                case GLFW_PRESS:
                    lOpenGLWindow->OnMouseButtonPressed(GLFWCodeToMouseButton(pButton), GLFWCodeToKeyboardMods(pMods));
                    break;
                case GLFW_RELEASE:
                    lOpenGLWindow->OnMouseButtonReleased(GLFWCodeToMouseButton(pButton), GLFWCodeToKeyboardMods(pMods));
                    break;
                }
            }
        });

    glfwSetKeyCallback(mWindow,
        [](GLFWwindow* pWindow, int pKey, int pScancode, int pAction, int pMods)
        {
            if (!ImGui::GetIO().WantCaptureKeyboard)
            {
                OpenGLWindow* lOpenGLWindow{ static_cast<OpenGLWindow*>(glfwGetWindowUserPointer(pWindow)) };
                switch (pAction)
                {
                case GLFW_PRESS:
                    lOpenGLWindow->OnKeyPressed(GLFWCodeToKeyboardKey(pKey), GLFWCodeToKeyboardMods(pMods));
                    break;
                case GLFW_REPEAT:
                    lOpenGLWindow->OnKeyRepeated(GLFWCodeToKeyboardKey(pKey), GLFWCodeToKeyboardMods(pMods));
                    break;
                case GLFW_RELEASE:
                    lOpenGLWindow->OnKeyReleased(GLFWCodeToKeyboardKey(pKey), GLFWCodeToKeyboardMods(pMods));
                    break;
                }
            }
        });

    ImGui::CreateContext();
    ImGui::StyleColorsDark();
    ImGui_ImplGlfw_InitForOpenGL(mWindow, true);
    ImGui_ImplOpenGL3_Init();

    if (OnCreate())
    {
        double lLastTime{ glfwGetTime() };
        while (!glfwWindowShouldClose(mWindow))
        {
            const double lCurrentTime{ glfwGetTime() };
            const double lElapsedTime{ lCurrentTime - lLastTime };
            lLastTime = lCurrentTime;

            ImGui_ImplOpenGL3_NewFrame();
            ImGui_ImplGlfw_NewFrame();
            ImGui::NewFrame();

            glfwPollEvents();

            if (!OnUpdate(lCurrentTime, lElapsedTime))
            {
                glfwSetWindowShouldClose(mWindow, GLFW_TRUE);
            }

            ImGui::Render();
            ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

            glfwSwapBuffers(mWindow);
        }
    }

    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    OnDestroy();
    glfwTerminate();
}

void OpenGLWindow::OnFramebufferResized(
    int pWidth,
    int pHeight)
{
    glViewport(0, 0, pWidth, pHeight);
}

void OpenGLWindow::OnKeyPressed(
    OpenGLWindow::KeyboardKey pKey,
    OpenGLWindow::KeyboardMods pMods)
{}

void OpenGLWindow::OnKeyReleased(
    OpenGLWindow::KeyboardKey pKey,
    OpenGLWindow::KeyboardMods pMods)
{}

void OpenGLWindow::OnKeyRepeated(
    OpenGLWindow::KeyboardKey pKey,
    OpenGLWindow::KeyboardMods pMods)
{}

void OpenGLWindow::OnMouseButtonPressed(
    OpenGLWindow::MouseButton pButton,
    OpenGLWindow::KeyboardMods pMods)
{}

void OpenGLWindow::OnMouseButtonReleased(
    OpenGLWindow::MouseButton pButton,
    OpenGLWindow::KeyboardMods pMods)
{}

void OpenGLWindow::OnMouseMoved(
    double pCursorX,
    double pCursorY)
{}

void OpenGLWindow::OnMouseScrolled(
    double pOffsetX,
    double pOffsetY)
{}

void OpenGLWindow::OnPathsDropped(
    const char** pPaths,
    std::uint32_t pCount)
{}

bool OpenGLWindow::IsIconified() const
{
    return mWindowIconified;
}

bool OpenGLWindow::IsCursorEnabled() const
{
    return glfwGetInputMode(mWindow, GLFW_CURSOR) == GLFW_CURSOR_NORMAL;
}

void OpenGLWindow::SetCursorEnabled(
    bool pEnabled)
{
    glfwSetInputMode(mWindow, GLFW_CURSOR, pEnabled ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_DISABLED);
}

bool OpenGLWindow::IsFramerateFixed() const
{
    return mWindowFramerateFixed;
}

void OpenGLWindow::SetFramerateFixed(
    bool pFixed)
{
    glfwSwapInterval((mWindowFramerateFixed = pFixed) ? 1 : 0);
}

int OpenGLWindow::GetFramebufferWidth() const
{
    return mWindowFramebufferWidth;
}

int OpenGLWindow::GetFramebufferHeight() const
{
    return mWindowFramebufferHeight;
}

double OpenGLWindow::GetCursorX() const
{
    double lCursorX, lCursorY;
    glfwGetCursorPos(mWindow, &lCursorX, &lCursorY);
    return lCursorX;
}

double OpenGLWindow::GetCursorY() const
{
    double lCursorX, lCursorY;
    glfwGetCursorPos(mWindow, &lCursorX, &lCursorY);
    return lCursorY;
}

bool OpenGLWindow::IsKeyPressed(
    OpenGLWindow::KeyboardKey pKey) const
{
    const int lState{ glfwGetKey(mWindow, KeyboardKeyToGLFWCode(pKey)) };
    return lState == GLFW_PRESS || lState == GLFW_REPEAT;
}

void OpenGLWindow::UserNotification() const
{
    glfwRequestWindowAttention(mWindow);
}

void OpenGLWindow::SetClipboardString(
    const std::string& pString)
{
    glfwSetClipboardString(mWindow, pString.c_str());
}

void OpenGLWindow::ImGuiShowWindowSettings()
{
    static int sSelectedMonitorIndex{};

    int lMonitorCount;
    if (GLFWmonitor** lMonitors{ glfwGetMonitors(&lMonitorCount) })
    {
        const char* lSelectedMonitorName{ glfwGetMonitorName(lMonitors[sSelectedMonitorIndex]) };
        if (ImGui::BeginCombo("Monitor", lSelectedMonitorName, ImGuiComboFlags_PopupAlignLeft))
        {
            for (int lCurrentMonitorIndex{}; lCurrentMonitorIndex < lMonitorCount; lCurrentMonitorIndex++)
            {
                const std::string lCurrentMonitorName{ std::format("{}. {}", lCurrentMonitorIndex + 1, glfwGetMonitorName(lMonitors[lCurrentMonitorIndex])) };
                const bool lIsSelected{ lCurrentMonitorIndex == sSelectedMonitorIndex };
                if (ImGui::Selectable(lCurrentMonitorName.c_str(), lIsSelected))
                {
                    sSelectedMonitorIndex = lCurrentMonitorIndex;

                    int lMonitorPositionX, lMonitorPositionY;
                    glfwGetMonitorPos(lMonitors[lCurrentMonitorIndex], &lMonitorPositionX, &lMonitorPositionY);
                    const GLFWvidmode* lVideoMode{ glfwGetVideoMode(lMonitors[lCurrentMonitorIndex]) };
                    glfwSetWindowMonitor(mWindow, nullptr, lMonitorPositionX, lMonitorPositionY, lVideoMode->width, lVideoMode->height, lVideoMode->refreshRate);
                }

                if (lIsSelected)
                {
                    ImGui::SetItemDefaultFocus();
                }
            }
            ImGui::EndCombo();
        }
    }
}

bool OpenGLWindow::IsMouseButtonPressed(
    OpenGLWindow::MouseButton pButton) const
{
    const int lState{ glfwGetMouseButton(mWindow, MouseButtonToGLFWCode(pButton)) };
    return lState == GLFW_PRESS;
}

bool OpenGLWindow::HasKeyboardMod(
    OpenGLWindow::KeyboardMods pMods,
    OpenGLWindow::KeyboardMod pMod)
{
    return pMods & static_cast<std::int32_t>(pMod);
}

namespace
{

OpenGLWindow::KeyboardKey GLFWCodeToKeyboardKey(
    std::int32_t pCode)
{
    switch (pCode)
    {
    case GLFW_KEY_UNKNOWN:
        return OpenGLWindow::KeyboardKey::KEY_UNKNOWN;
    case GLFW_KEY_SPACE:
        return OpenGLWindow::KeyboardKey::KEY_SPACE;
    case GLFW_KEY_APOSTROPHE:
        return OpenGLWindow::KeyboardKey::KEY_APOSTROPHE;
    case GLFW_KEY_COMMA:
        return OpenGLWindow::KeyboardKey::KEY_COMMA;
    case GLFW_KEY_MINUS:
        return OpenGLWindow::KeyboardKey::KEY_MINUS;
    case GLFW_KEY_PERIOD:
        return OpenGLWindow::KeyboardKey::KEY_PERIOD;
    case GLFW_KEY_SLASH:
        return OpenGLWindow::KeyboardKey::KEY_SLASH;
    case GLFW_KEY_0:
        return OpenGLWindow::KeyboardKey::KEY_0;
    case GLFW_KEY_1:
        return OpenGLWindow::KeyboardKey::KEY_1;
    case GLFW_KEY_2:
        return OpenGLWindow::KeyboardKey::KEY_2;
    case GLFW_KEY_3:
        return OpenGLWindow::KeyboardKey::KEY_3;
    case GLFW_KEY_4:
        return OpenGLWindow::KeyboardKey::KEY_4;
    case GLFW_KEY_5:
        return OpenGLWindow::KeyboardKey::KEY_5;
    case GLFW_KEY_6:
        return OpenGLWindow::KeyboardKey::KEY_6;
    case GLFW_KEY_7:
        return OpenGLWindow::KeyboardKey::KEY_7;
    case GLFW_KEY_8:
        return OpenGLWindow::KeyboardKey::KEY_8;
    case GLFW_KEY_9:
        return OpenGLWindow::KeyboardKey::KEY_9;
    case GLFW_KEY_SEMICOLON:
        return OpenGLWindow::KeyboardKey::KEY_SEMICOLON;
    case GLFW_KEY_EQUAL:
        return OpenGLWindow::KeyboardKey::KEY_EQUAL;
    case GLFW_KEY_A:
        return OpenGLWindow::KeyboardKey::KEY_A;
    case GLFW_KEY_B:
        return OpenGLWindow::KeyboardKey::KEY_B;
    case GLFW_KEY_C:
        return OpenGLWindow::KeyboardKey::KEY_C;
    case GLFW_KEY_D:
        return OpenGLWindow::KeyboardKey::KEY_D;
    case GLFW_KEY_E:
        return OpenGLWindow::KeyboardKey::KEY_E;
    case GLFW_KEY_F:
        return OpenGLWindow::KeyboardKey::KEY_F;
    case GLFW_KEY_G:
        return OpenGLWindow::KeyboardKey::KEY_G;
    case GLFW_KEY_H:
        return OpenGLWindow::KeyboardKey::KEY_H;
    case GLFW_KEY_I:
        return OpenGLWindow::KeyboardKey::KEY_I;
    case GLFW_KEY_J:
        return OpenGLWindow::KeyboardKey::KEY_J;
    case GLFW_KEY_K:
        return OpenGLWindow::KeyboardKey::KEY_K;
    case GLFW_KEY_L:
        return OpenGLWindow::KeyboardKey::KEY_L;
    case GLFW_KEY_M:
        return OpenGLWindow::KeyboardKey::KEY_M;
    case GLFW_KEY_N:
        return OpenGLWindow::KeyboardKey::KEY_N;
    case GLFW_KEY_O:
        return OpenGLWindow::KeyboardKey::KEY_O;
    case GLFW_KEY_P:
        return OpenGLWindow::KeyboardKey::KEY_P;
    case GLFW_KEY_Q:
        return OpenGLWindow::KeyboardKey::KEY_Q;
    case GLFW_KEY_R:
        return OpenGLWindow::KeyboardKey::KEY_R;
    case GLFW_KEY_S:
        return OpenGLWindow::KeyboardKey::KEY_S;
    case GLFW_KEY_T:
        return OpenGLWindow::KeyboardKey::KEY_T;
    case GLFW_KEY_U:
        return OpenGLWindow::KeyboardKey::KEY_U;
    case GLFW_KEY_V:
        return OpenGLWindow::KeyboardKey::KEY_V;
    case GLFW_KEY_W:
        return OpenGLWindow::KeyboardKey::KEY_W;
    case GLFW_KEY_X:
        return OpenGLWindow::KeyboardKey::KEY_X;
    case GLFW_KEY_Y:
        return OpenGLWindow::KeyboardKey::KEY_Y;
    case GLFW_KEY_Z:
        return OpenGLWindow::KeyboardKey::KEY_Z;
    case GLFW_KEY_LEFT_BRACKET:
        return OpenGLWindow::KeyboardKey::KEY_LEFT_BRACKET;
    case GLFW_KEY_BACKSLASH:
        return OpenGLWindow::KeyboardKey::KEY_BACKSLASH;
    case GLFW_KEY_RIGHT_BRACKET:
        return OpenGLWindow::KeyboardKey::KEY_RIGHT_BRACKET;
    case GLFW_KEY_GRAVE_ACCENT:
        return OpenGLWindow::KeyboardKey::KEY_GRAVE_ACCENT;
    case GLFW_KEY_WORLD_1:
        return OpenGLWindow::KeyboardKey::KEY_WORLD_1;
    case GLFW_KEY_WORLD_2:
        return OpenGLWindow::KeyboardKey::KEY_WORLD_2;
    case GLFW_KEY_ESCAPE:
        return OpenGLWindow::KeyboardKey::KEY_ESCAPE;
    case GLFW_KEY_ENTER:
        return OpenGLWindow::KeyboardKey::KEY_ENTER;
    case GLFW_KEY_TAB:
        return OpenGLWindow::KeyboardKey::KEY_TAB;
    case GLFW_KEY_BACKSPACE:
        return OpenGLWindow::KeyboardKey::KEY_BACKSPACE;
    case GLFW_KEY_INSERT:
        return OpenGLWindow::KeyboardKey::KEY_INSERT;
    case GLFW_KEY_DELETE:
        return OpenGLWindow::KeyboardKey::KEY_DELETE;
    case GLFW_KEY_RIGHT:
        return OpenGLWindow::KeyboardKey::KEY_RIGHT;
    case GLFW_KEY_LEFT:
        return OpenGLWindow::KeyboardKey::KEY_LEFT;
    case GLFW_KEY_DOWN:
        return OpenGLWindow::KeyboardKey::KEY_DOWN;
    case GLFW_KEY_UP:
        return OpenGLWindow::KeyboardKey::KEY_UP;
    case GLFW_KEY_PAGE_UP:
        return OpenGLWindow::KeyboardKey::KEY_PAGE_UP;
    case GLFW_KEY_PAGE_DOWN:
        return OpenGLWindow::KeyboardKey::KEY_PAGE_DOWN;
    case GLFW_KEY_HOME:
        return OpenGLWindow::KeyboardKey::KEY_HOME;
    case GLFW_KEY_END:
        return OpenGLWindow::KeyboardKey::KEY_END;
    case GLFW_KEY_CAPS_LOCK:
        return OpenGLWindow::KeyboardKey::KEY_CAPS_LOCK;
    case GLFW_KEY_SCROLL_LOCK:
        return OpenGLWindow::KeyboardKey::KEY_SCROLL_LOCK;
    case GLFW_KEY_NUM_LOCK:
        return OpenGLWindow::KeyboardKey::KEY_NUM_LOCK;
    case GLFW_KEY_PRINT_SCREEN:
        return OpenGLWindow::KeyboardKey::KEY_PRINT_SCREEN;
    case GLFW_KEY_PAUSE:
        return OpenGLWindow::KeyboardKey::KEY_PAUSE;
    case GLFW_KEY_F1:
        return OpenGLWindow::KeyboardKey::KEY_F1;
    case GLFW_KEY_F2:
        return OpenGLWindow::KeyboardKey::KEY_F2;
    case GLFW_KEY_F3:
        return OpenGLWindow::KeyboardKey::KEY_F3;
    case GLFW_KEY_F4:
        return OpenGLWindow::KeyboardKey::KEY_F4;
    case GLFW_KEY_F5:
        return OpenGLWindow::KeyboardKey::KEY_F5;
    case GLFW_KEY_F6:
        return OpenGLWindow::KeyboardKey::KEY_F6;
    case GLFW_KEY_F7:
        return OpenGLWindow::KeyboardKey::KEY_F7;
    case GLFW_KEY_F8:
        return OpenGLWindow::KeyboardKey::KEY_F8;
    case GLFW_KEY_F9:
        return OpenGLWindow::KeyboardKey::KEY_F9;
    case GLFW_KEY_F10:
        return OpenGLWindow::KeyboardKey::KEY_F10;
    case GLFW_KEY_F11:
        return OpenGLWindow::KeyboardKey::KEY_F11;
    case GLFW_KEY_F12:
        return OpenGLWindow::KeyboardKey::KEY_F12;
    case GLFW_KEY_F13:
        return OpenGLWindow::KeyboardKey::KEY_F13;
    case GLFW_KEY_F14:
        return OpenGLWindow::KeyboardKey::KEY_F14;
    case GLFW_KEY_F15:
        return OpenGLWindow::KeyboardKey::KEY_F15;
    case GLFW_KEY_F16:
        return OpenGLWindow::KeyboardKey::KEY_F16;
    case GLFW_KEY_F17:
        return OpenGLWindow::KeyboardKey::KEY_F17;
    case GLFW_KEY_F18:
        return OpenGLWindow::KeyboardKey::KEY_F18;
    case GLFW_KEY_F19:
        return OpenGLWindow::KeyboardKey::KEY_F19;
    case GLFW_KEY_F20:
        return OpenGLWindow::KeyboardKey::KEY_F20;
    case GLFW_KEY_F21:
        return OpenGLWindow::KeyboardKey::KEY_F21;
    case GLFW_KEY_F22:
        return OpenGLWindow::KeyboardKey::KEY_F22;
    case GLFW_KEY_F23:
        return OpenGLWindow::KeyboardKey::KEY_F23;
    case GLFW_KEY_F24:
        return OpenGLWindow::KeyboardKey::KEY_F24;
    case GLFW_KEY_F25:
        return OpenGLWindow::KeyboardKey::KEY_F25;
    case GLFW_KEY_KP_0:
        return OpenGLWindow::KeyboardKey::KEY_KP_0;
    case GLFW_KEY_KP_1:
        return OpenGLWindow::KeyboardKey::KEY_KP_1;
    case GLFW_KEY_KP_2:
        return OpenGLWindow::KeyboardKey::KEY_KP_2;
    case GLFW_KEY_KP_3:
        return OpenGLWindow::KeyboardKey::KEY_KP_3;
    case GLFW_KEY_KP_4:
        return OpenGLWindow::KeyboardKey::KEY_KP_4;
    case GLFW_KEY_KP_5:
        return OpenGLWindow::KeyboardKey::KEY_KP_5;
    case GLFW_KEY_KP_6:
        return OpenGLWindow::KeyboardKey::KEY_KP_6;
    case GLFW_KEY_KP_7:
        return OpenGLWindow::KeyboardKey::KEY_KP_7;
    case GLFW_KEY_KP_8:
        return OpenGLWindow::KeyboardKey::KEY_KP_8;
    case GLFW_KEY_KP_9:
        return OpenGLWindow::KeyboardKey::KEY_KP_9;
    case GLFW_KEY_KP_DECIMAL:
        return OpenGLWindow::KeyboardKey::KEY_KP_DECIMAL;
    case GLFW_KEY_KP_DIVIDE:
        return OpenGLWindow::KeyboardKey::KEY_KP_DIVIDE;
    case GLFW_KEY_KP_MULTIPLY:
        return OpenGLWindow::KeyboardKey::KEY_KP_MULTIPLY;
    case GLFW_KEY_KP_SUBTRACT:
        return OpenGLWindow::KeyboardKey::KEY_KP_SUBTRACT;
    case GLFW_KEY_KP_ADD:
        return OpenGLWindow::KeyboardKey::KEY_KP_ADD;
    case GLFW_KEY_KP_ENTER:
        return OpenGLWindow::KeyboardKey::KEY_KP_ENTER;
    case GLFW_KEY_KP_EQUAL:
        return OpenGLWindow::KeyboardKey::KEY_KP_EQUAL;
    case GLFW_KEY_LEFT_SHIFT:
        return OpenGLWindow::KeyboardKey::KEY_LEFT_SHIFT;
    case GLFW_KEY_LEFT_CONTROL:
        return OpenGLWindow::KeyboardKey::KEY_LEFT_CONTROL;
    case GLFW_KEY_LEFT_ALT:
        return OpenGLWindow::KeyboardKey::KEY_LEFT_ALT;
    case GLFW_KEY_LEFT_SUPER:
        return OpenGLWindow::KeyboardKey::KEY_LEFT_SUPER;
    case GLFW_KEY_RIGHT_SHIFT:
        return OpenGLWindow::KeyboardKey::KEY_RIGHT_SHIFT;
    case GLFW_KEY_RIGHT_CONTROL:
        return OpenGLWindow::KeyboardKey::KEY_RIGHT_CONTROL;
    case GLFW_KEY_RIGHT_ALT:
        return OpenGLWindow::KeyboardKey::KEY_RIGHT_ALT;
    case GLFW_KEY_RIGHT_SUPER:
        return OpenGLWindow::KeyboardKey::KEY_RIGHT_SUPER;
    case GLFW_KEY_MENU:
        return OpenGLWindow::KeyboardKey::KEY_MENU;
    }
    return OpenGLWindow::KeyboardKey::KEY_UNKNOWN;
}

OpenGLWindow::KeyboardMods GLFWCodeToKeyboardMods(
    std::int32_t pCode)
{
    std::int32_t lKeyboardMods{};
    if (pCode & GLFW_MOD_SHIFT)
    {
        lKeyboardMods |= static_cast<std::int32_t>(OpenGLWindow::KeyboardMod::MOD_SHIFT);
    }
    if (pCode & GLFW_MOD_CONTROL)
    {
        lKeyboardMods |= static_cast<std::int32_t>(OpenGLWindow::KeyboardMod::MOD_CONTROL);
    }
    if (pCode & GLFW_MOD_ALT)
    {
        lKeyboardMods |= static_cast<std::int32_t>(OpenGLWindow::KeyboardMod::MOD_ALT);
    }
    if (pCode & GLFW_MOD_SUPER)
    {
        lKeyboardMods |= static_cast<std::int32_t>(OpenGLWindow::KeyboardMod::MOD_SUPER);
    }
    if (pCode & GLFW_MOD_CAPS_LOCK)
    {
        lKeyboardMods |= static_cast<std::int32_t>(OpenGLWindow::KeyboardMod::MOD_CAPS_LOCK);
    }
    if (pCode & GLFW_MOD_NUM_LOCK)
    {
        lKeyboardMods |= static_cast<std::int32_t>(OpenGLWindow::KeyboardMod::MOD_NUM_LOCK);
    }
    return lKeyboardMods;
}

OpenGLWindow::MouseButton GLFWCodeToMouseButton(
    std::int32_t pCode)
{
    switch (pCode)
    {
    case GLFW_MOUSE_BUTTON_1:
        return OpenGLWindow::MouseButton::BUTTON_1;
    case GLFW_MOUSE_BUTTON_2:
        return OpenGLWindow::MouseButton::BUTTON_2;
    case GLFW_MOUSE_BUTTON_3:
        return OpenGLWindow::MouseButton::BUTTON_3;
    case GLFW_MOUSE_BUTTON_4:
        return OpenGLWindow::MouseButton::BUTTON_4;
    case GLFW_MOUSE_BUTTON_5:
        return OpenGLWindow::MouseButton::BUTTON_5;
    case GLFW_MOUSE_BUTTON_6:
        return OpenGLWindow::MouseButton::BUTTON_6;
    case GLFW_MOUSE_BUTTON_7:
        return OpenGLWindow::MouseButton::BUTTON_7;
    case GLFW_MOUSE_BUTTON_8:
        return OpenGLWindow::MouseButton::BUTTON_8;
    }
    return OpenGLWindow::MouseButton::BUTTON_1;
}

std::int32_t KeyboardKeyToGLFWCode(
    OpenGLWindow::KeyboardKey pCode)
{
    switch (pCode)
    {
    case OpenGLWindow::KeyboardKey::KEY_UNKNOWN:
        return GLFW_KEY_UNKNOWN;
    case OpenGLWindow::KeyboardKey::KEY_SPACE:
        return GLFW_KEY_SPACE;
    case OpenGLWindow::KeyboardKey::KEY_APOSTROPHE:
        return GLFW_KEY_APOSTROPHE;
    case OpenGLWindow::KeyboardKey::KEY_COMMA:
        return GLFW_KEY_COMMA;
    case OpenGLWindow::KeyboardKey::KEY_MINUS:
        return GLFW_KEY_MINUS;
    case OpenGLWindow::KeyboardKey::KEY_PERIOD:
        return GLFW_KEY_PERIOD;
    case OpenGLWindow::KeyboardKey::KEY_SLASH:
        return GLFW_KEY_SLASH;
    case OpenGLWindow::KeyboardKey::KEY_0:
        return GLFW_KEY_0;
    case OpenGLWindow::KeyboardKey::KEY_1:
        return GLFW_KEY_1;
    case OpenGLWindow::KeyboardKey::KEY_2:
        return GLFW_KEY_2;
    case OpenGLWindow::KeyboardKey::KEY_3:
        return GLFW_KEY_3;
    case OpenGLWindow::KeyboardKey::KEY_4:
        return GLFW_KEY_4;
    case OpenGLWindow::KeyboardKey::KEY_5:
        return GLFW_KEY_5;
    case OpenGLWindow::KeyboardKey::KEY_6:
        return GLFW_KEY_6;
    case OpenGLWindow::KeyboardKey::KEY_7:
        return GLFW_KEY_7;
    case OpenGLWindow::KeyboardKey::KEY_8:
        return GLFW_KEY_8;
    case OpenGLWindow::KeyboardKey::KEY_9:
        return GLFW_KEY_9;
    case OpenGLWindow::KeyboardKey::KEY_SEMICOLON:
        return GLFW_KEY_SEMICOLON;
    case OpenGLWindow::KeyboardKey::KEY_EQUAL:
        return GLFW_KEY_EQUAL;
    case OpenGLWindow::KeyboardKey::KEY_A:
        return GLFW_KEY_A;
    case OpenGLWindow::KeyboardKey::KEY_B:
        return GLFW_KEY_B;
    case OpenGLWindow::KeyboardKey::KEY_C:
        return GLFW_KEY_C;
    case OpenGLWindow::KeyboardKey::KEY_D:
        return GLFW_KEY_D;
    case OpenGLWindow::KeyboardKey::KEY_E:
        return GLFW_KEY_E;
    case OpenGLWindow::KeyboardKey::KEY_F:
        return GLFW_KEY_F;
    case OpenGLWindow::KeyboardKey::KEY_G:
        return GLFW_KEY_G;
    case OpenGLWindow::KeyboardKey::KEY_H:
        return GLFW_KEY_H;
    case OpenGLWindow::KeyboardKey::KEY_I:
        return GLFW_KEY_I;
    case OpenGLWindow::KeyboardKey::KEY_J:
        return GLFW_KEY_J;
    case OpenGLWindow::KeyboardKey::KEY_K:
        return GLFW_KEY_K;
    case OpenGLWindow::KeyboardKey::KEY_L:
        return GLFW_KEY_L;
    case OpenGLWindow::KeyboardKey::KEY_M:
        return GLFW_KEY_M;
    case OpenGLWindow::KeyboardKey::KEY_N:
        return GLFW_KEY_N;
    case OpenGLWindow::KeyboardKey::KEY_O:
        return GLFW_KEY_O;
    case OpenGLWindow::KeyboardKey::KEY_P:
        return GLFW_KEY_P;
    case OpenGLWindow::KeyboardKey::KEY_Q:
        return GLFW_KEY_Q;
    case OpenGLWindow::KeyboardKey::KEY_R:
        return GLFW_KEY_R;
    case OpenGLWindow::KeyboardKey::KEY_S:
        return GLFW_KEY_S;
    case OpenGLWindow::KeyboardKey::KEY_T:
        return GLFW_KEY_T;
    case OpenGLWindow::KeyboardKey::KEY_U:
        return GLFW_KEY_U;
    case OpenGLWindow::KeyboardKey::KEY_V:
        return GLFW_KEY_V;
    case OpenGLWindow::KeyboardKey::KEY_W:
        return GLFW_KEY_W;
    case OpenGLWindow::KeyboardKey::KEY_X:
        return GLFW_KEY_X;
    case OpenGLWindow::KeyboardKey::KEY_Y:
        return GLFW_KEY_Y;
    case OpenGLWindow::KeyboardKey::KEY_Z:
        return GLFW_KEY_Z;
    case OpenGLWindow::KeyboardKey::KEY_LEFT_BRACKET:
        return GLFW_KEY_LEFT_BRACKET;
    case OpenGLWindow::KeyboardKey::KEY_BACKSLASH:
        return GLFW_KEY_BACKSLASH;
    case OpenGLWindow::KeyboardKey::KEY_RIGHT_BRACKET:
        return GLFW_KEY_RIGHT_BRACKET;
    case OpenGLWindow::KeyboardKey::KEY_GRAVE_ACCENT:
        return GLFW_KEY_GRAVE_ACCENT;
    case OpenGLWindow::KeyboardKey::KEY_WORLD_1:
        return GLFW_KEY_WORLD_1;
    case OpenGLWindow::KeyboardKey::KEY_WORLD_2:
        return GLFW_KEY_WORLD_2;
    case OpenGLWindow::KeyboardKey::KEY_ESCAPE:
        return GLFW_KEY_ESCAPE;
    case OpenGLWindow::KeyboardKey::KEY_ENTER:
        return GLFW_KEY_ENTER;
    case OpenGLWindow::KeyboardKey::KEY_TAB:
        return GLFW_KEY_TAB;
    case OpenGLWindow::KeyboardKey::KEY_BACKSPACE:
        return GLFW_KEY_BACKSPACE;
    case OpenGLWindow::KeyboardKey::KEY_INSERT:
        return GLFW_KEY_INSERT;
    case OpenGLWindow::KeyboardKey::KEY_DELETE:
        return GLFW_KEY_DELETE;
    case OpenGLWindow::KeyboardKey::KEY_RIGHT:
        return GLFW_KEY_RIGHT;
    case OpenGLWindow::KeyboardKey::KEY_LEFT:
        return GLFW_KEY_LEFT;
    case OpenGLWindow::KeyboardKey::KEY_DOWN:
        return GLFW_KEY_DOWN;
    case OpenGLWindow::KeyboardKey::KEY_UP:
        return GLFW_KEY_UP;
    case OpenGLWindow::KeyboardKey::KEY_PAGE_UP:
        return GLFW_KEY_PAGE_UP;
    case OpenGLWindow::KeyboardKey::KEY_PAGE_DOWN:
        return GLFW_KEY_PAGE_DOWN;
    case OpenGLWindow::KeyboardKey::KEY_HOME:
        return GLFW_KEY_HOME;
    case OpenGLWindow::KeyboardKey::KEY_END:
        return GLFW_KEY_END;
    case OpenGLWindow::KeyboardKey::KEY_CAPS_LOCK:
        return GLFW_KEY_CAPS_LOCK;
    case OpenGLWindow::KeyboardKey::KEY_SCROLL_LOCK:
        return GLFW_KEY_SCROLL_LOCK;
    case OpenGLWindow::KeyboardKey::KEY_NUM_LOCK:
        return GLFW_KEY_NUM_LOCK;
    case OpenGLWindow::KeyboardKey::KEY_PRINT_SCREEN:
        return GLFW_KEY_PRINT_SCREEN;
    case OpenGLWindow::KeyboardKey::KEY_PAUSE:
        return GLFW_KEY_PAUSE;
    case OpenGLWindow::KeyboardKey::KEY_F1:
        return GLFW_KEY_F1;
    case OpenGLWindow::KeyboardKey::KEY_F2:
        return GLFW_KEY_F2;
    case OpenGLWindow::KeyboardKey::KEY_F3:
        return GLFW_KEY_F3;
    case OpenGLWindow::KeyboardKey::KEY_F4:
        return GLFW_KEY_F4;
    case OpenGLWindow::KeyboardKey::KEY_F5:
        return GLFW_KEY_F5;
    case OpenGLWindow::KeyboardKey::KEY_F6:
        return GLFW_KEY_F6;
    case OpenGLWindow::KeyboardKey::KEY_F7:
        return GLFW_KEY_F7;
    case OpenGLWindow::KeyboardKey::KEY_F8:
        return GLFW_KEY_F8;
    case OpenGLWindow::KeyboardKey::KEY_F9:
        return GLFW_KEY_F9;
    case OpenGLWindow::KeyboardKey::KEY_F10:
        return GLFW_KEY_F10;
    case OpenGLWindow::KeyboardKey::KEY_F11:
        return GLFW_KEY_F11;
    case OpenGLWindow::KeyboardKey::KEY_F12:
        return GLFW_KEY_F12;
    case OpenGLWindow::KeyboardKey::KEY_F13:
        return GLFW_KEY_F13;
    case OpenGLWindow::KeyboardKey::KEY_F14:
        return GLFW_KEY_F14;
    case OpenGLWindow::KeyboardKey::KEY_F15:
        return GLFW_KEY_F15;
    case OpenGLWindow::KeyboardKey::KEY_F16:
        return GLFW_KEY_F16;
    case OpenGLWindow::KeyboardKey::KEY_F17:
        return GLFW_KEY_F17;
    case OpenGLWindow::KeyboardKey::KEY_F18:
        return GLFW_KEY_F18;
    case OpenGLWindow::KeyboardKey::KEY_F19:
        return GLFW_KEY_F19;
    case OpenGLWindow::KeyboardKey::KEY_F20:
        return GLFW_KEY_F20;
    case OpenGLWindow::KeyboardKey::KEY_F21:
        return GLFW_KEY_F21;
    case OpenGLWindow::KeyboardKey::KEY_F22:
        return GLFW_KEY_F22;
    case OpenGLWindow::KeyboardKey::KEY_F23:
        return GLFW_KEY_F23;
    case OpenGLWindow::KeyboardKey::KEY_F24:
        return GLFW_KEY_F24;
    case OpenGLWindow::KeyboardKey::KEY_F25:
        return GLFW_KEY_F25;
    case OpenGLWindow::KeyboardKey::KEY_KP_0:
        return GLFW_KEY_KP_0;
    case OpenGLWindow::KeyboardKey::KEY_KP_1:
        return GLFW_KEY_KP_1;
    case OpenGLWindow::KeyboardKey::KEY_KP_2:
        return GLFW_KEY_KP_2;
    case OpenGLWindow::KeyboardKey::KEY_KP_3:
        return GLFW_KEY_KP_3;
    case OpenGLWindow::KeyboardKey::KEY_KP_4:
        return GLFW_KEY_KP_4;
    case OpenGLWindow::KeyboardKey::KEY_KP_5:
        return GLFW_KEY_KP_5;
    case OpenGLWindow::KeyboardKey::KEY_KP_6:
        return GLFW_KEY_KP_6;
    case OpenGLWindow::KeyboardKey::KEY_KP_7:
        return GLFW_KEY_KP_7;
    case OpenGLWindow::KeyboardKey::KEY_KP_8:
        return GLFW_KEY_KP_8;
    case OpenGLWindow::KeyboardKey::KEY_KP_9:
        return GLFW_KEY_KP_9;
    case OpenGLWindow::KeyboardKey::KEY_KP_DECIMAL:
        return GLFW_KEY_KP_DECIMAL;
    case OpenGLWindow::KeyboardKey::KEY_KP_DIVIDE:
        return GLFW_KEY_KP_DIVIDE;
    case OpenGLWindow::KeyboardKey::KEY_KP_MULTIPLY:
        return GLFW_KEY_KP_MULTIPLY;
    case OpenGLWindow::KeyboardKey::KEY_KP_SUBTRACT:
        return GLFW_KEY_KP_SUBTRACT;
    case OpenGLWindow::KeyboardKey::KEY_KP_ADD:
        return GLFW_KEY_KP_ADD;
    case OpenGLWindow::KeyboardKey::KEY_KP_ENTER:
        return GLFW_KEY_KP_ENTER;
    case OpenGLWindow::KeyboardKey::KEY_KP_EQUAL:
        return GLFW_KEY_KP_EQUAL;
    case OpenGLWindow::KeyboardKey::KEY_LEFT_SHIFT:
        return GLFW_KEY_LEFT_SHIFT;
    case OpenGLWindow::KeyboardKey::KEY_LEFT_CONTROL:
        return GLFW_KEY_LEFT_CONTROL;
    case OpenGLWindow::KeyboardKey::KEY_LEFT_ALT:
        return GLFW_KEY_LEFT_ALT;
    case OpenGLWindow::KeyboardKey::KEY_LEFT_SUPER:
        return GLFW_KEY_LEFT_SUPER;
    case OpenGLWindow::KeyboardKey::KEY_RIGHT_SHIFT:
        return GLFW_KEY_RIGHT_SHIFT;
    case OpenGLWindow::KeyboardKey::KEY_RIGHT_CONTROL:
        return GLFW_KEY_RIGHT_CONTROL;
    case OpenGLWindow::KeyboardKey::KEY_RIGHT_ALT:
        return GLFW_KEY_RIGHT_ALT;
    case OpenGLWindow::KeyboardKey::KEY_RIGHT_SUPER:
        return GLFW_KEY_RIGHT_SUPER;
    case OpenGLWindow::KeyboardKey::KEY_MENU:
        return GLFW_KEY_MENU;
    }
    return GLFW_KEY_UNKNOWN;
}

std::int32_t MouseButtonToGLFWCode(
    OpenGLWindow::MouseButton pCode)
{
    switch (pCode)
    {
    case OpenGLWindow::MouseButton::BUTTON_1:
        return GLFW_MOUSE_BUTTON_1;
    case OpenGLWindow::MouseButton::BUTTON_2:
        return GLFW_MOUSE_BUTTON_2;
    case OpenGLWindow::MouseButton::BUTTON_3:
        return GLFW_MOUSE_BUTTON_3;
    case OpenGLWindow::MouseButton::BUTTON_4:
        return GLFW_MOUSE_BUTTON_4;
    case OpenGLWindow::MouseButton::BUTTON_5:
        return GLFW_MOUSE_BUTTON_5;
    case OpenGLWindow::MouseButton::BUTTON_6:
        return GLFW_MOUSE_BUTTON_6;
    case OpenGLWindow::MouseButton::BUTTON_7:
        return GLFW_MOUSE_BUTTON_7;
    case OpenGLWindow::MouseButton::BUTTON_8:
        return GLFW_MOUSE_BUTTON_8;
    }
    return GLFW_MOUSE_BUTTON_1;
}

} // anonymous namespace

} // AoE2DE::Overlay::Graphics