///////////////////////////////////////////////////////////////////////////////////////////////////
// OpenGL Shader Implementation                                                                  //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <AoE2DE/Overlay/Graphics/OpenGLShader.hpp>

// -------------------------------------------------------------------------------- Library Headers
#include <glad/gl.h>

#include <glm/gtc/type_ptr.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

namespace AoE2DE::Overlay::Graphics
{

namespace
{

std::string ReadShaderSource(
    const std::string& pFilename)
{
    if (std::ifstream lFile{ std::ifstream(pFilename) })
    {
        return (std::stringstream{} << lFile.rdbuf()).str();
    }
    return {};
}

bool CheckShaderCompileStatus(
    std::uint32_t pShaderHandle)
{
    GLint lCompileStatus;
    glGetShaderiv(pShaderHandle, GL_COMPILE_STATUS, &lCompileStatus);

    GLint lInfoLogLength;
    glGetShaderiv(pShaderHandle, GL_INFO_LOG_LENGTH, &lInfoLogLength);

    if (lInfoLogLength)
    {
        std::string lInfoLog(lInfoLogLength, 0);
        glGetShaderInfoLog(pShaderHandle, lInfoLogLength, nullptr, lInfoLog.data());

        std::cerr << lInfoLog << std::endl;
    }

    return lCompileStatus;
}

bool CheckProgramLinkStatus(
    std::uint32_t pProgramHandle)
{
    GLint lLinkStatus;
    glGetProgramiv(pProgramHandle, GL_LINK_STATUS, &lLinkStatus);

    GLint lInfoLogLength;
    glGetProgramiv(pProgramHandle, GL_INFO_LOG_LENGTH, &lInfoLogLength);

    if (lInfoLogLength)
    {
        std::string lInfoLog(lInfoLogLength, 0);
        glGetProgramInfoLog(pProgramHandle, lInfoLogLength, nullptr, lInfoLog.data());

        std::cerr << lInfoLog << std::endl;
    }

    return lLinkStatus;
}

} // anonymous namespace

OpenGLShader::OpenGLShader(
    const std::filesystem::path& pFilepath) :
    mHandle(0)
{
    std::string lVertexShaderSource{ ReadShaderSource(pFilepath.generic_string() + ".vert")};
    std::string lFragmentShaderSource{ ReadShaderSource(pFilepath.generic_string() + ".frag") };

    GLuint lVertexShaderHandle{ glCreateShader(GL_VERTEX_SHADER) };
    char* lVertexShaderSourceData{ lVertexShaderSource.data() };
    glShaderSource(lVertexShaderHandle, 1, &lVertexShaderSourceData, 0);
    glCompileShader(lVertexShaderHandle);
    if (!CheckShaderCompileStatus(lVertexShaderHandle))
    {
        glDeleteShader(lVertexShaderHandle);
        return;
    }

    GLuint lFragmentShaderHandle{ glCreateShader(GL_FRAGMENT_SHADER) };
    char* lFragmentShaderSourceData{ lFragmentShaderSource.data() };
    glShaderSource(lFragmentShaderHandle, 1, &lFragmentShaderSourceData, 0);
    glCompileShader(lFragmentShaderHandle);
    if (!CheckShaderCompileStatus(lFragmentShaderHandle))
    {
        glDeleteShader(lVertexShaderHandle);
        glDeleteShader(lFragmentShaderHandle);
        return;
    }

    mHandle = glCreateProgram();
    glAttachShader(mHandle, lVertexShaderHandle);
    glAttachShader(mHandle, lFragmentShaderHandle);
    glLinkProgram(mHandle);
    if (!CheckProgramLinkStatus(mHandle))
    {
        glDeleteShader(lVertexShaderHandle);
        glDeleteShader(lFragmentShaderHandle);
        glDeleteProgram(mHandle);
        mHandle = 0;
        return;
    }

    glDeleteShader(lVertexShaderHandle);
    glDeleteShader(lFragmentShaderHandle);
}

OpenGLShader::~OpenGLShader()
{
    glDeleteProgram(mHandle);
}

bool OpenGLShader::IsNull() const
{
    return !mHandle;
}

void OpenGLShader::Use() const
{
    glUseProgram(mHandle);
}

void OpenGLShader::SetTransform(
    const glm::mat4& pTransform)
{
    const GLint lTransformLocation{ glGetUniformLocation(mHandle, "uTransform") };
    glUniformMatrix4fv(lTransformLocation, 1, GL_FALSE, glm::value_ptr(pTransform));
}

void OpenGLShader::SetProjection(
    const glm::mat4& pProjection)
{
    const GLint lProjectionLocation{ glGetUniformLocation(mHandle, "uProjection") };
    glUniformMatrix4fv(lProjectionLocation, 1, GL_FALSE, glm::value_ptr(pProjection));
}

} // AoE2DE::Overlay::Graphics