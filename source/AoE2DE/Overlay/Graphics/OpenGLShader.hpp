#ifndef AOE2DE_OVERLAY_GRAPHICS_OPENGL_SHADER_HPP_INCLUDED
#define AOE2DE_OVERLAY_GRAPHICS_OPENGL_SHADER_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// OpenGL Shader Interface                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Library Headers
#include <glm/glm.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <cstdint>
#include <filesystem>

namespace AoE2DE::Overlay::Graphics
{

/**
 * @brief OpenGL shader.
 */
class OpenGLShader
{
public:
    /**
     * @brief Constructor.
     *
     * Create an OpenGL shader from source GSLS vertex and fragment shader files.
     * Upon failure, the resulting OpenGL shader is null.
     *
     * @param pFilepath The base path of the GSLS vertex ans fragment shader files.
     */
    OpenGLShader(
        const std::filesystem::path& pFilepath);

    /**
     * @brief Destructor.
     */
    virtual ~OpenGLShader();

    /**
     * @brief Deleted copy constructor.
     */
    OpenGLShader(const OpenGLShader&) = delete;

    /**
     * @brief Deleted move constructor.
     */
    OpenGLShader(OpenGLShader&&) = delete;

    /**
     * @brief Deleted copy-assignment operator.
     */
    OpenGLShader& operator=(const OpenGLShader&) = delete;

    /**
     * @brief Deleted move-assignment operator.
     */
    OpenGLShader& operator=(OpenGLShader&&) = delete;

    /**
     * @brief Test if the OpenGL shader is null.
     *
     * The OpenGL shader is null if it failed construction.
     *
     * @return True if the OpenGL shader is null.
     */
    bool IsNull() const;

    /**
     * @brief Use the OpenGL shader in the next draw call.
     */
    void Use() const;

    /**
     * @brief Set the object transform.
     *
     * @param pTransform The object transform.
     */
    void SetTransform(
        const glm::mat4& pTransform);

    /**
     * @brief Set the projection matrix.
     *
     * @param pProjection The projection matrix.
     */
    void SetProjection(
        const glm::mat4& pProjection);

protected:
    /**
     * @brief Internal OpenGL shader handle.
     */
    std::uint32_t mHandle;
};

} // AoE2DE::Overlay::Graphics

#endif // AOE2DE_OVERLAY_GRAPHICS_OPENGL_SHADER_HPP_INCLUDED