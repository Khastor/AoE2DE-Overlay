///////////////////////////////////////////////////////////////////////////////////////////////////
// OpenGL Unit Shader Implementation                                                             //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <AoE2DE/Overlay/Graphics/OpenGLUnitShader.hpp>

// -------------------------------------------------------------------------------- Library Headers
#include <glad/gl.h>

#include <glm/gtc/type_ptr.hpp>

namespace AoE2DE::Overlay::Graphics
{

OpenGLUnitShader::OpenGLUnitShader() :
    OpenGLShader("assets/shaders/Unit")
{}

void OpenGLUnitShader::SetTextureSize(
    const glm::vec2& pTextureSize)
{
    const GLint lTextureSizeLocation{ glGetUniformLocation(mHandle, "uTextureSize") };
    glUniform2fv(lTextureSizeLocation, 1, glm::value_ptr(pTextureSize));
}

void OpenGLUnitShader::SetTextureHotspot(
    const glm::vec2& pTextureHotspot)
{
    const GLint lTextureHotspotLocation{ glGetUniformLocation(mHandle, "uTextureHotspot") };
    glUniform2fv(lTextureHotspotLocation, 1, glm::value_ptr(pTextureHotspot));
}

} // AoE2DE::Overlay::Graphics