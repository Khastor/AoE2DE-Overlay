#version 460 core

layout (location = 0) in vec2 aVertexCoord;
layout (location = 1) in vec2 aVertexTextureCoord;
layout (location = 2) in vec4 aInstancePosition;
layout (location = 3) in vec3 aInstanceColor;

out vec3 gTextureCoord;
out vec3 gPlayerColor;

uniform mat4 uProjection;
uniform vec2 uTextureSize;
uniform vec2 uTextureHotspot;

void main()
{
    const float lScale = aInstancePosition.w;
    gl_Position = uProjection * vec4(aInstancePosition.xy - uTextureHotspot.xy * lScale + vec2(aVertexCoord * uTextureSize) * lScale, aInstancePosition.y, 1.0);
    gTextureCoord = vec3(aVertexTextureCoord.xy, aInstancePosition.z);
    gPlayerColor = aInstanceColor;
}