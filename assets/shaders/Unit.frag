#version 460 core

in vec3 gTextureCoord;
in vec3 gPlayerColor;
out vec4 gFragmentColor;

layout (binding = 0) uniform sampler2DArray uColorTexture;
layout (binding = 1) uniform sampler2DArray uPlayerMaskTexture;
layout (binding = 2) uniform sampler2DArray uShadowTexture;

void main()
{
    vec4 lColor = texture(uColorTexture, gTextureCoord);
    float lShadow = texture(uShadowTexture, gTextureCoord).r;
    float lPlayerMask = texture(uPlayerMaskTexture, gTextureCoord).r;
    gFragmentColor = mix(lColor, lColor * vec4(gPlayerColor, 1.0), lPlayerMask) + vec4(0.0, 0.0, 0.0, lShadow);

    if (gFragmentColor.a < 0.01)
    {
        discard;
    }
}