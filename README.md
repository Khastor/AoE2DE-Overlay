# Age of Empires II Definitive Edition Overlay

Interactive Twitch stream overlay for Age of Empires II Definitive Edition.

Age of Empires II � Microsoft Corporation. AoE2DEOverlay was created under Microsoft's "[Game Content Usage Rules](https://www.xbox.com/en-us/developers/rules)" using assets from Age of Empires II, and it is not endorsed by or affiliated with Microsoft.

## Dependencies

* [freetype2](https://freetype.org)
* [glad](https://github.com/Dav1dde/glad)
* [glfw](https://github.com/glfw/glfw)
* [glm](https://github.com/g-truc/glm)
* [imgui](https://github.com/ocornut/imgui)
* [stb_image](https://github.com/nothings/stb)

## Submodules

* [Khastor/AoE2DE-Core](https://gitlab.com/Khastor/AoE2DE-Core)
* [Khastor/Network](https://gitlab.com/Khastor/Network)

## Build with CMake

```
$ mkdir build
$ cd build
$ cmake ..
$ cmake --build .
$ cmake --install .
```